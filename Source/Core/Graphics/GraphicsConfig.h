/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GRAPHICS_GRAPHICS_CONFIG_H
#define GRAPHICS_GRAPHICS_CONFIG_H

#include <stdint.h>

namespace HT
{
enum class ExponentialLevel : uint8_t
{
	NONE,
	X2,
	X4,
	X8,
	X16
};

enum class Quality : uint8_t
{
	MINIMAL,
	LOW,
	MEDIUM,
	HIGH,
	ULTRA
};

enum class RendererType : uint8_t
{
	Auto,
	OpenGL,
	Vulkan,
#if defined(__WIN32__)
	Direct3D11,
	Direct3D12,
#elif defined(OSX)
	Metal,
#endif // defined
	None
};

enum class WindowMode : uint8_t
{
	WINDOWED,
	FULLSCREEN,
	BORDERLESS
};

struct Resolution
{
	uint16_t width;
	uint16_t height;
	uint16_t refresh_rate;
};

struct GPU
{
	uint16_t device_id;
	uint16_t vendor_id;
};

struct WindowPlatformData
{
	void* window;
	void* display;
};

struct RendererConfig
{
	Resolution resolution;
	GPU gpu;
	RendererType renderer;
	WindowMode window_mode;
	Quality models_quality;
	Quality shaders_quality;
	Quality materials_quality;
	Quality textures_quality;
	ExponentialLevel msaa;
	ExponentialLevel anisotropy;
	bool vsync;
};

struct WindowConfig
{
	const char* title;
	WindowMode window_mode;
	Resolution resolution;
	uint16_t refresh_rate;
	bool vsync;
};
} // namespace HT

#endif // GRAPHICS_GRAPHICS_CONFIG_H
