/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GRAPHICS_RENDERER_PLUGIN_H
#define GRAPHICS_RENDERER_PLUGIN_H

#include "HellTech3DCoreAPI.h"
#include "Plugin/PluginInterface.h"

namespace HT
{
struct GPU;
struct RendererConfig;
struct RenderQueue;
struct WindowPlatformData;

class HELLTECH3DCOREAPI_EXPORT RendererPlugin : public PluginInterface
{
public:
	RendererPlugin() = default;
	RendererPlugin(RendererPlugin&&) noexcept = default;
	RendererPlugin& operator=(RendererPlugin&&) noexcept = default;
	virtual ~RendererPlugin() = default;

	virtual bool IsGpuSelectible() const = 0;
	virtual bool SelectBestGpu(GPU& gpu) const = 0;

	virtual bool Initialize(const RendererConfig& config, const WindowPlatformData& main_wpd) = 0;
	virtual void Reconfigure(const RendererConfig& config) = 0;
	virtual void Shutdown() = 0;

	virtual bool RenderFrame(RenderQueue& queue) = 0;

	PluginType GetPluginType() const override final { return PluginType::RENDERER; }
};
} // namespace HT

#endif // GRAPHICS_RENDERER_PLUGIN_H
