/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GRAPHICS_WINDOW_INTERFACE_H
#define GRAPHICS_WINDOW_INTERFACE_H

#include "HellTech3DCoreAPI.h"
#include "Plugin/PluginInterface.h"

namespace HT
{
struct WindowConfig;
struct WindowPlatformData;

class HELLTECH3DCOREAPI_EXPORT WindowPlugin : public PluginInterface
{
public:
	WindowPlugin() = default;
	virtual ~WindowPlugin() = default;

	WindowPlugin(WindowPlugin&&) noexcept = default;
	WindowPlugin& operator=(WindowPlugin&&) noexcept = default;

	PluginType GetPluginType() const override final { return PluginType::WINDOW; }

	virtual bool Initialize() = 0;
	virtual void Shutdown() = 0;

	virtual bool Create(const WindowConfig& init_config) = 0;
	virtual bool Reconfigure(const WindowConfig& new_config) = 0;
	virtual void Destroy() = 0;

	virtual bool GetPlatformData(WindowPlatformData& wpd) = 0;
};
} // namespace HT

#endif // GRAPHICS_WINDOW_INTERFACE_H
