/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <iterator>
#include "CorePluginFactoriesStore.h"
#include "Plugin/PluginEntry.h"
#include "Plugin/PluginFactory.h"

namespace HT
{
CorePluginFactoriesStore::CorePluginFactoriesStore() :
	factories_{},
	libraries_{}
{
}

void CorePluginFactoriesStore::AddLibrary(
	const PluginEntry* plugin_entry,
	HT_SharedLibrary library
)
{
	unsigned position;
	for (const PluginFactory** factory_ptr = plugin_entry->factories;
		 factory_ptr < plugin_entry->factories + plugin_entry->factories_count;
		 ++factory_ptr)
	{
		position = static_cast<unsigned>((*factory_ptr)->GetPluginType());
		factories_[position] = *factory_ptr;
		libraries_[position] = library;
	}
}

void CorePluginFactoriesStore::RemoveLibrary(HT_SharedLibrary library)
{
	const PluginFactory** factory_ptr = std::begin(factories_);
	HT_SharedLibrary* library_ptr = &libraries_[0];
	for (; factory_ptr < std::end(factories_); ++factory_ptr, ++library_ptr)
		if (*library_ptr == library)
		{
			*factory_ptr = nullptr;
			*library_ptr = nullptr;
		}
}

void CorePluginFactoriesStore::RemoveAll()
{
	memset(factories_, 0, CORE_PLUGINS_COUNT);
	memset(libraries_, 0, CORE_PLUGINS_COUNT);
}
} // namespace HT
