/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_CORE_PLUGIN_FACTORIES_STORE_H
#define PLUGIN_CORE_PLUGIN_FACTORIES_STORE_H

#include <stdint.h>
#include "Plugin/PluginDefs.h"

#ifdef LoadLibrary
#undef LoadLibrary
#endif // LoadLibrary

using HT_SharedLibrary = void*;

namespace HT
{
class PluginEntry;
class PluginFactory;
class PluginInterface;

class CorePluginFactoriesStore
{
public:
	CorePluginFactoriesStore();

	CorePluginFactoriesStore(CorePluginFactoriesStore&&) noexcept = default;
	CorePluginFactoriesStore& operator=(CorePluginFactoriesStore&&) noexcept = default;

	void AddLibrary(const PluginEntry* plugin_entry, HT_SharedLibrary library);
	void RemoveLibrary(HT_SharedLibrary library);
	void RemoveAll();

	const PluginFactory* const* Factories() const { return factories_; }
	const HT_SharedLibrary* Handles() { return libraries_; }
	static constexpr unsigned Count() { return CORE_PLUGINS_COUNT; }

private:
	static constexpr unsigned CORE_PLUGINS_COUNT = static_cast<unsigned>(PluginType::LOGIC);

	const PluginFactory* factories_[CORE_PLUGINS_COUNT];
	HT_SharedLibrary libraries_[CORE_PLUGINS_COUNT];

private:
	CorePluginFactoriesStore(const CorePluginFactoriesStore&) = delete;
	CorePluginFactoriesStore& operator=(const CorePluginFactoriesStore&) = delete;
};
} // namespace HT

#endif // PLUGIN_CORE_PLUGIN_FACTORIES_STORE_H
