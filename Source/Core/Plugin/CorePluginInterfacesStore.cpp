/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include <memory>
#include <new>
#include "CorePluginInterfacesStore.h"
#include "Plugin/PluginInterface.h"
#include "Plugin/PluginFactory.h"

namespace HT
{
CorePluginInterfacesStore::CorePluginInterfacesStore() :
	interfaces_{},
	memory_(nullptr)
{
}

CorePluginInterfacesStore::CorePluginInterfacesStore(CorePluginInterfacesStore&& other) noexcept :
	memory_(other.memory_)
{
	memcpy(interfaces_, other.interfaces_, sizeof(*interfaces_) * CORE_PLUGINS_COUNT);
	memset(other.interfaces_, 0, sizeof(*interfaces_) * CORE_PLUGINS_COUNT);

	other.memory_ = nullptr;
}

void CorePluginInterfacesStore::operator=(CorePluginInterfacesStore&& other) noexcept
{
	RemoveAll();

	memcpy(interfaces_, other.interfaces_, sizeof(*interfaces_) * CORE_PLUGINS_COUNT);
	memset(other.interfaces_, 0, sizeof(*interfaces_) * CORE_PLUGINS_COUNT);

	memory_ = other.memory_;
	other.memory_ = nullptr;
}

CorePluginInterfacesStore::~CorePluginInterfacesStore()
{
	RemoveAll();
}

bool CorePluginInterfacesStore::CreateInitial(const PluginFactory* const* factories, unsigned count)
{
	if (memory_ != nullptr)
		return false;

	const PluginFactory* const* factory;
	const PluginFactory* const* factories_end = factories + count;

	size_t objects_size = 0;
	for (factory = factories; factory < factories_end; ++factory)
		if (*factory != nullptr)
			objects_size += (*factory)->GetObjectSize();
	
	memory_ = Allocate(objects_size);
	if (memory_ == nullptr)
		return false;

	uint8_t* memory_ptr = reinterpret_cast<uint8_t*>(memory_);
	PluginInterface** interface = interfaces_;
	PluginInterface* interface_memory;
	for (factory = factories; factory < factories_end; ++factory, ++interface)
		if ((*factory) != nullptr)
		{
			interface_memory = reinterpret_cast<PluginInterface*>(memory_ptr);
			memory_ptr += (*factory)->GetObjectSize();
			(*factory)->Create(interface_memory);
			*interface = interface_memory;
		}

	return true;
}

void CorePluginInterfacesStore::RemoveAll()
{
	if (memory_ != nullptr)
	{
		for (PluginInterface** interface = interfaces_;
			interface < interfaces_ + CORE_PLUGINS_COUNT;
			++interface)
			std::destroy_at(interface);
		
		memset(interfaces_, 0, sizeof(*interfaces_) * CORE_PLUGINS_COUNT);

		Deallocate(memory_);
		memory_ = nullptr;
	}
}

void* CorePluginInterfacesStore::Allocate(size_t all_objects_size)
{
	return ::operator new(all_objects_size, std::nothrow);
}

void CorePluginInterfacesStore::Deallocate(void* memory)
{
	::operator delete(memory);
}
} // namespace HT
