/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_CORE_PLUGIN_INTERFACES_STORE_H
#define PLUGIN_CORE_PLUGIN_INTERFACES_STORE_H

#include <stdint.h>
#include "CorePluginFactoriesStore.h"
#include "Plugin/PluginDefs.h"

#ifdef LoadLibrary
#undef LoadLibrary
#endif // LoadLibrary

using HT_SharedLibrary = void*;

namespace HT
{
class PluginFactory;
class PluginInterface;

class CorePluginInterfacesStore
{
public:
	CorePluginInterfacesStore();
	CorePluginInterfacesStore(CorePluginInterfacesStore&& other) noexcept;
	void operator=(CorePluginInterfacesStore&& other) noexcept;
	~CorePluginInterfacesStore();

	bool CreateInitial(const PluginFactory* const* factories, unsigned count);
	void RemoveAll();

	PluginInterface* operator[](PluginType plugin_type) const
	{
		return interfaces_[static_cast<unsigned>(plugin_type)];
	}

private:
	static constexpr unsigned CORE_PLUGINS_COUNT = static_cast<unsigned>(PluginType::LOGIC);

	PluginInterface* interfaces_[CORE_PLUGINS_COUNT];
	void* memory_;

	// No need to be inlined: dynamic memory MUCH slower than function call.
	static void* Allocate(size_t all_objects_size);
	static void Deallocate(void* memory);

	static constexpr const unsigned OBJECTS_COUNT = static_cast<unsigned>(PluginType::LOGIC);

private:
	CorePluginInterfacesStore(const CorePluginInterfacesStore&) = delete;
	CorePluginInterfacesStore& operator=(const CorePluginInterfacesStore&) = delete;
};
} // namespace HT

#endif // PLUGIN_CORE_PLUGIN_INTERFACES_STORE_H
