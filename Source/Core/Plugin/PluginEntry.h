/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_PLUGIN_ENTRY_H
#define PLUGIN_PLUGIN_ENTRY_H

#include "PluginFactory.h"

#define HT_PLUGIN_ENTRY_FUNCTION GetPluginEntryStruct
#define HT_PLUGIN_ENTRY_FUNCTION_NAME "GetPluginEntryStruct"

namespace HT
{
struct PluginEntry
{
	const HT::PluginFactory** factories;
	const size_t factories_count;

	PluginEntry(const HT::PluginFactory** _factories, size_t _factories_count) :
		factories(_factories),
		factories_count(_factories_count)
	{
	}

private:
	PluginEntry(const PluginEntry&) = delete;
	void operator=(const PluginEntry&) = delete;
};
} // namespace HT

#endif // PLUGIN_PLUGIN_ENTRY_H
