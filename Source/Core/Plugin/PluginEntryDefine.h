/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_PLUGIN_ENTRY_DEFINE_H
#define PLUGIN_PLUGIN_ENTRY_DEFINE_H

#include <memory>
#include "PluginEntry.h"

#ifndef HT_PLUGIN_EXPORT
#error Declare HT_PLUGIN_EXPORT macro as plugin function export modifier.
#endif // HT_PLUGIN_EXPORT

#define HT_PLUGIN_FACTORIES_BEGIN() \
namespace PluginFactories \
{ \
using Registry = HT::Detail::PluginFactoriesRegistry<>

#define HT_PLUGIN_FACTORY(PLUGIN_CLASS, PLUGIN_TYPE) ::Add<HT::PluginFactoryImpl<PLUGIN_CLASS, PLUGIN_TYPE>>

#define HT_PLUGIN_FACTORIES_END() ; \
static const PluginFactory* g_PluginFactories[Registry::ARRAY_SIZE]; \
static Registry g_PluginFactoriesRegistry; \
static HT::Detail::PluginFactoryRegistrator<Registry> g_PluginFactoriesInit(g_PluginFactoriesRegistry, g_PluginFactories); \
static const HT::PluginEntry g_PluginEntry(g_PluginFactories, Registry::ARRAY_SIZE); \
} \
extern "C" HT_PLUGIN_EXPORT const HT::PluginEntry* HT_PLUGIN_ENTRY_FUNCTION() { return &PluginFactories::g_PluginEntry; }

namespace HT
{
class PluginInterface;

template <typename TPluginInterface, PluginType PLUGIN_TYPE>
struct PluginFactoryImpl final : PluginFactory
{
	void Create(PluginInterface* ptr) const override
	{
		TPluginInterface* casted_ptr = static_cast<TPluginInterface*>(ptr);
		std::construct_at(casted_ptr);
	}

	size_t GetObjectSize() const noexcept override { return sizeof(TPluginInterface); }
	virtual PluginType GetPluginType() const noexcept override { return PLUGIN_TYPE; };
};

namespace Detail
{
template <typename...> struct PluginFactoriesRegistry;

template <typename TPluginFactory, typename... TOther>
struct PluginFactoriesRegistry<TPluginFactory, TOther...>
{
	template <typename TPluginFactoryNext>
	using Add = PluginFactoriesRegistry<TPluginFactory, TOther..., TPluginFactoryNext>;

	enum { ARRAY_SIZE = PluginFactoriesRegistry<TOther...>::ARRAY_SIZE + 1 };

	void Fill(const PluginFactory** factories) const
	{
		*factories = &factory;
		other.Fill(factories + 1);
	}

	const TPluginFactory factory;
	PluginFactoriesRegistry<TOther...> other;
};

template <typename TPluginFactory>
struct PluginFactoriesRegistry<TPluginFactory>
{
	template <typename TPluginFactoryNext>
	using Add = PluginFactoriesRegistry<TPluginFactory, TPluginFactoryNext>;

	enum { ARRAY_SIZE = 1 };

	void Fill(const PluginFactory** factories) const
	{
		*factories = &factory;
	}

	const TPluginFactory factory;
};

template <>
struct PluginFactoriesRegistry<>
{
	template <typename TPluginFactoryNext>
	using Add = PluginFactoriesRegistry<TPluginFactoryNext>;

	void Fill([[maybe_unused]] const PluginFactory** factories) const {}
	enum { ARRAY_SIZE = 0 };
};

template <typename TPluginFactoriesRegistry>
struct PluginFactoryRegistrator
{
	PluginFactoryRegistrator(const TPluginFactoriesRegistry& registry, const PluginFactory** factories)
	{
		registry.Fill(factories);
	}
};
} // namespace Detail
} // namespace HT

#endif // PLUGIN_PLUGIN_ENTRY_H
