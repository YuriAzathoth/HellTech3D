/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_PLUGIN_FACTORY_H
#define PLUGIN_PLUGIN_FACTORY_H

#include <stddef.h>
#include "PluginDefs.h"

namespace HT
{
class PluginInterface;

class PluginFactory
{
public:
	PluginFactory() = default;
	virtual ~PluginFactory() = default;

	virtual void Create(PluginInterface* ptr) const = 0;
	virtual size_t GetObjectSize() const noexcept = 0;
	virtual PluginType GetPluginType() const noexcept = 0;

private:
	PluginFactory(const PluginFactory&) = delete;
	void operator=(const PluginFactory&) = delete;
};
} // namespace HT

#endif // PLUGIN_PLUGIN_FACTORY_H
