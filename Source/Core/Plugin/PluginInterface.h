/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_PLUGIN_INTERFACE_H
#define PLUGIN_PLUGIN_INTERFACE_H

#include "HellTech3DCoreAPI.h"
#include "PluginDefs.h"

namespace HT
{
class HELLTECH3DCOREAPI_EXPORT PluginInterface
{
public:
	PluginInterface() = default;
	virtual ~PluginInterface() = default;

	virtual PluginType GetPluginType() const = 0;

	PluginInterface(PluginInterface&&) noexcept = default;
	PluginInterface& operator=(PluginInterface&&) noexcept = default;

private:
	PluginInterface(const PluginInterface&) = delete;
	PluginInterface& operator=(const PluginInterface&) = delete;
};
} // namespace HT

#endif // PLUGIN_PLUGIN_INTERFACE_H
