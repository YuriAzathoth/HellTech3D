/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PluginsStore.h"

namespace HT
{
bool PluginsStore::InitializeCore(const char** filenames, unsigned count)
{
	const SharedLibraryData* lib;
	for (const char** filename_ptr = filenames;
		 filename_ptr < filenames + count;
		 ++filename_ptr)
	{
		lib = libs_.Load(*filename_ptr);
		if (lib == nullptr)
		{
			libs_.ForceCloseAll();
			core_factories_.RemoveAll();
			return false;
		}
		
		core_factories_.AddLibrary(lib->entry, lib->handle);
	}

	if (!core_interfaces_.CreateInitial(core_factories_.Factories(), core_factories_.Count()))
	{
		libs_.ForceCloseAll();
		core_factories_.RemoveAll();
		return false;
	}

	for (const HT_SharedLibrary* lib_ptr = core_factories_.Handles();
		 lib_ptr < core_factories_.Handles() + core_factories_.Count();
		 ++lib_ptr)
		libs_.AcquireUsing(*lib_ptr);

	return true;
}

void PluginsStore::ReleaseRedundant()
{
	libs_.CloseUnused();
}

void PluginsStore::RemoveAll()
{
	core_interfaces_.RemoveAll();
	core_factories_.RemoveAll();
	libs_.ForceCloseAll();
}
} // namespace HT
