/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_PLUGINS_STORE_H
#define PLUGIN_PLUGINS_STORE_H

#include "CorePluginFactoriesStore.h"
#include "CorePluginInterfacesStore.h"
#include "PluginDefs.h"
#include "PluginInterface.h"
#include "SharedLibrariesStore.h"

namespace HT
{
class PluginsStore
{
public:
	PluginsStore() = default;
	~PluginsStore() = default;

	bool InitializeCore(const char** filenames, unsigned count);
	void ReleaseRedundant();

	void RemoveAll();

	PluginInterface* GetInterface(PluginType type) { return core_interfaces_[type]; }
	const PluginInterface* GetInterface(PluginType type) const { return core_interfaces_[type]; }

	template <typename T>
	T* GetInterface(PluginType type) const
	{
		return static_cast<T*>(core_interfaces_[type]);
	}

	PluginsStore(PluginsStore&&) noexcept = default;
	PluginsStore& operator=(PluginsStore&&) noexcept = default;

private:
	SharedLibrariesStore libs_;
	CorePluginFactoriesStore core_factories_;
	CorePluginInterfacesStore core_interfaces_;

	PluginsStore(const PluginsStore&) = delete;
	PluginsStore& operator=(const PluginsStore&) = delete;
};
} // namespace HT

#endif // PLUGIN_PLUGINS_STORE_H
