/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include "Plugin/PluginEntry.h"
#include "SharedLibrariesStore.h"
#include "System/FileSystem.h"
#include "System/SharedLibrary.h"

namespace HT
{
const SharedLibraryData*
SharedLibrariesStore::GetByName(const std::string& filename)
{
	auto library_it = FindItByFilename(filename);
	if (library_it != libraries_.end() && library_it->filename == filename)
		return &library_it->meta;
	else
		return nullptr;
}

/*const SharedLibraryData*
SharedLibrariesStore::GetByHadle(HT_SharedLibrary library)
{
	auto library_it = FindLibraryByHandle(library);
	if (library_it != libraries_.end())
		return &library_it->meta;
	else
		return nullptr;
}

const std::string&
SharedLibrariesStore::GetFilename(HT_SharedLibrary library)
{
	// Static object to return empty string when not found.
	static std::string EMPTY = "";

	auto library_it = FindLibraryByHandle(library);
	if (library_it != libraries_.end())
		return library_it->filename;
	else
		return EMPTY;
}*/

const SharedLibraryData*
SharedLibrariesStore::Load(const std::string& filename)
{
	auto library_it = FindItByFilename(filename);
	if (library_it != libraries_.end() && library_it->filename == filename)
		return &library_it->meta;

	LibraryRecord record;
	record.filename = filename;
	if (!LoadSharedLibrary(record))
		return nullptr;

	library_it = libraries_.insert(library_it, std::move(record));

	return &library_it->meta;
}

void SharedLibrariesStore::AcquireUsing(HT_SharedLibrary library)
{
	auto library_it = FindItByHandle(library);
	if (library_it != libraries_.end())
		++library_it->meta.refs;
}

void SharedLibrariesStore::ReleaseUsing(HT_SharedLibrary library)
{
	auto library_it = FindItByHandle(library);
	if (library_it != libraries_.end())
		--library_it->meta.refs;
}

void SharedLibrariesStore::CloseUnused()
{
	auto first_it = std::remove_if(
		libraries_.begin(),
		libraries_.end(),
		[](const LibraryRecord& record) { return record.meta.refs == 0; }
	);

	for (auto it = first_it; it != libraries_.end(); ++it)
		CloseSharedLibrary(*it);

	libraries_.erase(first_it, libraries_.end());
}

void SharedLibrariesStore::ForceClose(const std::string& filename)
{
	auto library_it = FindItByFilename(filename);
	if (library_it != libraries_.end() && library_it->filename == filename)
	{
		CloseSharedLibrary(*library_it);
		libraries_.erase(library_it);
	}
}

void SharedLibrariesStore::ForceClose(HT_SharedLibrary library)
{
	auto library_it = FindItByHandle(library);
	if (library_it != libraries_.end())
	{
		CloseSharedLibrary(*library_it);
		libraries_.erase(library_it);
	}
}

void SharedLibrariesStore::ForceCloseAll()
{
	for (LibraryRecord& library : libraries_)
		HT_CloseSharedLibrary(library.meta.handle);
	libraries_.clear();
}

void SharedLibrariesStore::Reserve(unsigned size)
{
	libraries_.reserve(static_cast<size_t>(size));
}

SharedLibrariesStore::RecordsVector::iterator
SharedLibrariesStore::FindItByFilename(const std::string& filename)
{
	return std::lower_bound(
		libraries_.begin(),
		libraries_.end(),
		filename,
		[](const LibraryRecord& lhs, const std::string& rhs)
		{
			return lhs.filename < rhs;
		});
}

SharedLibrariesStore::RecordsVector::iterator
SharedLibrariesStore::FindItByHandle(HT_SharedLibrary handle)
{
	return std::find_if(
		libraries_.begin(),
		libraries_.end(),
		[handle](const LibraryRecord& lhs)
		{
			return lhs.meta.handle == handle;
		});
}

bool SharedLibrariesStore::LoadSharedLibrary(LibraryRecord& record)
{
	char full_filename[HT_PLUGIN_MAX_PATH_SIZE];

#ifdef HT_PLATFORM_MULTI_ARCH
	const char* branch_suffix = HT_GetMicroArchitectureSuffix(HT_PLATFORM_BRANCH_ARCHITECTURE);
	if (!HT_SetSharedLibraryFullFilename(full_filename,
										 HT_PLUGIN_MAX_PATH_SIZE,
										 HT_GetProgramPath(),
										 HT_GetProgramPathSize(),
										 record.filename.c_str(),
										 record.filename.length(),
										 branch_suffix,
										 strlen(branch_suffix),
										 0))
		return false;
#else // HT_PLATFORM_MULTI_ARCH
	if (!HT_SetSharedLibraryFullFilename(full_filename,
										 HT_PLUGIN_MAX_PATH_SIZE,
										 HT_GetProgramPath(),
										 HT_GetProgramPathSize(),
										 record.filename.c_str(),
										 record.filename.length(),
										 "",
										 0,
										 0))
		return false;
#endif // HT_PLATFORM_MULTI_ARCH

	HT_SharedLibrary library = HT_LoadSharedLibrary(full_filename);
	if (library == nullptr)
		return false;

	using GetPluginEntryFN = const HT::PluginEntry*();
	GetPluginEntryFN* GetPluginEntry =
		reinterpret_cast<GetPluginEntryFN*>(HT_GetSharedLibrarySymbol(
			library,
			HT_PLUGIN_ENTRY_FUNCTION_NAME
		));
	if (GetPluginEntry == nullptr)
	{
		HT_CloseSharedLibrary(library);
		return false;
	}

	const PluginEntry* entry = (*GetPluginEntry)();
	if (entry == nullptr)
	{
		HT_CloseSharedLibrary(library);
		return false;
	}

	// record.meta.filename MUST be set BEFORE this function call.
	record.meta.handle = library;
	record.meta.entry = entry;
	record.meta.refs = 0;

	return true;
}

void SharedLibrariesStore::CloseSharedLibrary(LibraryRecord& record)
{
	HT_CloseSharedLibrary(record.meta.handle);
}
} // namespace HT
