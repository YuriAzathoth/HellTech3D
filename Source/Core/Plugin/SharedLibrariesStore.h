/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_SHARED_LIBRARIES_STORE_H
#define PLUGIN_SHARED_LIBRARIES_STORE_H

#include <stdint.h>
#include <string>
#include <vector>
#include "System/SharedLibrary.h"

#ifdef LoadLibrary
#undef LoadLibrary
#endif // LoadLibrary

using HT_SharedLibrary = void*;

namespace HT
{
class PluginEntry;

struct SharedLibraryData
{
	HT_SharedLibrary handle;
	const PluginEntry* entry;
	uint32_t refs;
};

class SharedLibrariesStore
{
public:
	SharedLibrariesStore() {}
	~SharedLibrariesStore() { ForceCloseAll(); }

	SharedLibrariesStore(SharedLibrariesStore&&) noexcept = default;
	SharedLibrariesStore& operator=(SharedLibrariesStore&&) noexcept = default;

	const SharedLibraryData* GetByName(const std::string& filename);
	/*const SharedLibraryMetadata* GetByHadle(HT_SharedLibrary library);
	const std::string& GetFilename(HT_SharedLibrary library);*/

	const SharedLibraryData* Load(const std::string& filename);

	void AcquireUsing(HT_SharedLibrary library);
	void ReleaseUsing(HT_SharedLibrary library);
	void CloseUnused();

	void ForceClose(const std::string& filename);
	void ForceClose(HT_SharedLibrary library);
	void ForceCloseAll();

	void Reserve(unsigned size);

private:
	struct LibraryRecord
	{
		std::string filename;
		SharedLibraryData meta;
	};

	using RecordsVector = std::vector<LibraryRecord>;

	RecordsVector::iterator FindItByFilename(const std::string& filename);
	RecordsVector::iterator FindItByHandle(HT_SharedLibrary handle);

	RecordsVector libraries_;

	static bool LoadSharedLibrary(LibraryRecord& record);
	static void CloseSharedLibrary(LibraryRecord& record);

private:
	SharedLibrariesStore(const SharedLibrariesStore&) = delete;
	SharedLibrariesStore& operator=(const SharedLibrariesStore&) = delete;
};
} // namespace HT

#endif // PLUGIN_SHARED_LIBRARIES_STORE_H
