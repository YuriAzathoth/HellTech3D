/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <thread>
#include "EngineLauncher.h"
#include "Graphics/GraphicsConfig.h"
#include "Graphics/RendererPlugin.h"
#include "Graphics/WindowPlugin.h"
#include "HellTech3DCoreAPI.h"
#include "Plugin/PluginDefs.h"

namespace HT
{
EngineLauncher::EngineLauncher()
{
}

EngineLauncher::~EngineLauncher()
{
}

bool EngineLauncher::Run(int argc, const char** argv, const char* library_filename, const char* window_title)
{
	const char* CORE_PLUGINS[] = { "HellTech3DSystem", "HellTech3DRenderer_BGFX" };

	if (!plugins_.InitializeCore(CORE_PLUGINS, 2))
		return false;

	plugins_.ReleaseRedundant();

	WindowConfig window_cfg{};
	window_cfg.title = window_title;
	window_cfg.window_mode = WindowMode::WINDOWED;
	window_cfg.resolution.width = 800;
	window_cfg.resolution.height = 600;
	window_cfg.refresh_rate = 60;
	window_cfg.vsync = false;

	WindowPlugin* window = plugins_.GetInterface<WindowPlugin>(PluginType::WINDOW);
	RendererPlugin* renderer = plugins_.GetInterface<RendererPlugin>(PluginType::RENDERER);
	if (window == nullptr && renderer == nullptr)
		return false;

	if (!window->Initialize())
		return false;
	
	if (!window->Create(window_cfg))
		return false;
	
	WindowPlatformData wpd{};
	if (!window->GetPlatformData(wpd))
		return false;

	RendererConfig renderer_cfg{};
	renderer_cfg.window_mode = WindowMode::WINDOWED;
	renderer_cfg.resolution.width = window_cfg.resolution.width;
	renderer_cfg.resolution.height = window_cfg.resolution.height;
	renderer_cfg.renderer = RendererType::OpenGL;
	renderer_cfg.vsync = false;

	if (renderer->IsGpuSelectible())
		if (renderer->SelectBestGpu(renderer_cfg.gpu))
			return false;

	if (!renderer->Initialize(renderer_cfg, wpd))
		return false;

	flecs::world world;
	world.set_threads(std::thread::hardware_concurrency());

	RenderQueue* rq = nullptr; // RenderQueue is not implemented yet.

	bool run = true;
	while (run)
	{
		world.progress();

		run &= renderer->RenderFrame(rq);

		/*GraphicsInterface* graphics = plugins_.GetInterface<GraphicsInterface>(PluginType::GRAPHICS);
		graphics->RenderFrame(world);*/
	}

	renderer->Shutdown();

	window->Destroy();
	window->Shutdown();

	plugins_.RemoveAll();

	return true;
}
} // namespace HT

extern "C"
HELLTECH3DCOREAPI_EXPORT
int StartGame(
	int argc,
	const char** argv,
	const char* library_filename,
	const char* window_title)
{
	HT::EngineLauncher engine;
	return engine.Run(argc, argv, library_filename, window_title) ? 0 : -1;
}
