/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SHELL_ENGINE_LAUNCHER_H
#define SHELL_ENGINE_LAUNCHER_H

#include <flecs.h>
#include "Plugin/PluginsStore.h"

namespace HT
{
class EngineLauncher
{
public:
	EngineLauncher();
	~EngineLauncher();

	bool Run(int argc, const char** argv, const char* library_filename, const char* window_title);

private:
	PluginsStore plugins_;
	flecs::world world_;

	EngineLauncher(const EngineLauncher&) = delete;
	void operator=(const EngineLauncher&) = delete;
};
} // namespace HT

#endif // SHELL_ENGINE_LAUNCHER_H
