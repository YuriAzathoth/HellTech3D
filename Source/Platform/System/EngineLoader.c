/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <locale.h>
#include <stdio.h>
#include <string.h>
#include "EngineLoader.h"
#include "FileSystem.h"
#include "MicroArchitecture.h"
#include "SharedLibrary.h"

#define HT_SHARED_LIBRARY_MAX_PATH_SIZE 512

#define HT_ENGINE_LIB_FILE_NAME "HellTech3DCore"
#define HT_ENGINE_LAUNCH_FN_NAME "StartGame"
typedef int(*HT_StartEnginePFN)(int, const char**, const char*, const char*);

static HT_SharedLibrary LoadLibraryImpl(const char* filename, enum HT_Architectures arch)
{
	char full_filename[HT_SHARED_LIBRARY_MAX_PATH_SIZE];

	const char* arch_suffix = HT_GetMicroArchitectureSuffix(arch);

	if (HT_SetSharedLibraryFullFilename(full_filename,
										HT_SHARED_LIBRARY_MAX_PATH_SIZE,
										HT_GetProgramPath(),
										HT_GetProgramPathSize(),
										filename,
										strlen(filename),
										arch_suffix,
										strlen(arch_suffix),
										HT_SHARED_LIBRARY_FORCE_BRANCH) == NULL)
	{
		fprintf(stderr,
				"FATAL ERROR: Library \"%s\" filename exceeds max buffer size.\n",
				filename);
		return NULL;
	}

	HT_SharedLibrary lib = HT_LoadSharedLibrary(full_filename);
	if (lib != NULL)
		return lib;
	else
	{
		fprintf(stderr, "FATAL ERROR: Library \"%s\" does not exist.\n", full_filename);
		return NULL;
	}
}

int HT_LaunchEngine(int argc, const char** argv, const char* gamelib_filename, const char* window_title)
{
	setlocale(LC_ALL, "");

	enum HT_Architectures arch = HT_ARCHITECTURE_NONE;

	{
#ifdef HT_MULTI_ARCHITECTURES
		int arch_auto_detect = 1;
#endif // HT_MULTI_ARCHITECTURES

		// TODO: Command args parsing.
		/*for (const char** argp = &argv[1]; argp < argv + argc; ++argp)
		{
		}*/

#ifdef HT_MULTI_ARCHITECTURES
		if (arch_auto_detect != 0)
		{
			arch = HT_DetectMicroArchitecture();
		}
#endif // HT_MULTI_ARCHITECTURES
	}

	HT_SharedLibrary client_lib = LoadLibraryImpl(HT_ENGINE_LIB_FILE_NAME, arch);
	if (client_lib == NULL)
		return -1;

	HT_StartEnginePFN StartEngine =
		(HT_StartEnginePFN)HT_GetSharedLibrarySymbol(
			client_lib,
			HT_ENGINE_LAUNCH_FN_NAME
		);

	if (StartEngine == NULL)
	{
		HT_CloseSharedLibrary(client_lib);
		fprintf(stderr,
				"FATAL ERROR: Library \""
				HT_ENGINE_LIB_FILE_NAME
				"\" does not have symbol \""
				HT_ENGINE_LAUNCH_FN_NAME
				"\".\n");
		return -1;
	}

	int res = (*StartEngine)(argc, argv, gamelib_filename, window_title);
	HT_CloseSharedLibrary(client_lib);

	return (res == 0) ? 0 : res - 10;
}
