/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SYSTEM_ENGINE_LOADER_H
#define SYSTEM_ENGINE_LOADER_H

#ifdef __cplusplus
#	define EXTERN_C_IF_CPP extern "C"
#else // __cplusplus
#	define EXTERN_C_IF_CPP
#endif // __cplusplus

#ifdef WIN32
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#	define HT_USE_DISCRETE_GPU() \
		EXTERN_C_IF_CPP __declspec(dllexport) DWORD NvOptimusEnablement = 1; \
		EXTERN_C_IF_CPP __declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
#else // WIN32
#	define HT_USE_DISCRETE_GPU()
#endif // WIN32

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

int HT_LaunchEngine(int argc, const char** argv, const char* gamelib_filename, const char* window_title);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // SYSTEM_ENGINE_LOADER_H
