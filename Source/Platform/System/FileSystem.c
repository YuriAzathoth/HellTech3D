/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "FileSystem.h"

static unsigned GetProgramFullFilename(char* dst);

#ifndef __EMSCRIPTEN__
#define HT_PROGRAM_PATH_MAX_SIZE 1024

struct HT_FileSystemGlobals
{
	char program_path[HT_PROGRAM_PATH_MAX_SIZE];
	unsigned program_path_size;
};

static struct HT_FileSystemGlobals g_Globals = { "", 0 };

static unsigned FindFilenameStartPos(const char* filename, unsigned size);
static void CheckAndSetProgramPath();

const char* HT_GetProgramPath()
{
	CheckAndSetProgramPath();
	return g_Globals.program_path;
}

unsigned HT_GetProgramPathSize()
{
	CheckAndSetProgramPath();
	return g_Globals.program_path_size;
}

static unsigned FindFilenameStartPos(const char* filename, unsigned size)
{
	const char* buf_ptr = filename + size - 1;
	while (buf_ptr > filename && *buf_ptr != '/' && *buf_ptr != '\\')
		--buf_ptr;
	return buf_ptr - filename + 1; // We need next position after last '/'.
}

static void CheckAndSetProgramPath()
{
	if (g_Globals.program_path_size == 0)
	{
		const unsigned path_size = GetProgramFullFilename(g_Globals.program_path);
		const unsigned pos = FindFilenameStartPos(g_Globals.program_path, path_size);
		if (pos > 0)
		{
			g_Globals.program_path_size = pos;
			g_Globals.program_path[pos] = '\0';
		}
	}
}

#else // __EMSCRIPTEN__

const char* HT_GetProgramPath()
{
	return "";
}

unsigned HT_GetProgramPathSize()
{
	return 0;
}

#endif // __EMSCRIPTEN__

#if defined(__WIN32__)
#include <fileapi.h>
#include <libloaderapi.h>

static unsigned GetProgramFullFilename(char* dst)
{
	return (unsigned)GetModuleFileName(NULL, dst, HT_PROGRAM_PATH_MAX_SIZE);
}

int HT_IsFileExists(const char* filename)
{
	const DWORD attrib = GetFileAttributes(filename);
	return (attrib != INVALID_FILE_ATTRIBUTES) && ((attrib & FILE_ATTRIBUTE_DIRECTORY) == 0);
}

int HT_IsDirExists(const char* dirpath)
{
	const DWORD attrib = GetFileAttributes(dirpath);
	return (attrib != INVALID_FILE_ATTRIBUTES) && ((attrib & FILE_ATTRIBUTE_DIRECTORY) != 0);
}

#else // defined
#include <sys/stat.h>
#include <unistd.h>

#ifndef __EMSCRIPTEN__
static unsigned GetProgramFullFilename(char* dst)
{
	const int size = readlink("/proc/self/exe", dst, HT_PROGRAM_PATH_MAX_SIZE);
	if (size >= 0)
		dst[size] = '\0';
	return (size >= 0) ? (unsigned)size : 0u;
}
#endif // __EMSCRIPTEN__

int HT_IsFileExists(const char* filename)
{
	struct stat sb;
	if (stat(filename, &sb) == 0)
		return 0;
	else
		return S_ISREG(sb.st_mode);
}

int HT_IsDirExists(const char* dirpath)
{
	struct stat sb;
	if (stat(dirpath, &sb) == 0)
		return 0;
	else
		return S_ISDIR(sb.st_mode);
}

#endif // defined
