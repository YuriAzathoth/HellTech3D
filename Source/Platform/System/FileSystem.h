/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SYSTEM_FILE_SYSTEM_H
#define SYSTEM_FILE_SYSTEM_H

enum HT_FileType
{
	HT_FILE_TYPE_NONE,
	HT_FILE_TYPE_FILE,
	HT_FILE_TYPE_DIRECTORY
};

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

const char* HT_GetProgramPath();
unsigned HT_GetProgramPathSize();

int HT_IsFileExists(const char* filename);
int HT_IsDirExists(const char* dirpath);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // SYSTEM_FILE_SYSTEM_H
