/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// WARNING! Only x86 architecture both 32 and 64 bit is supported!
// ARM and probably other architectures will be added latter.

#include "MicroArchitecture.h"

#ifdef HT_MULTI_ARCHITECTURES

#include <stdio.h>
#include <libcpuid.h>

#	if HT_PLATFORM_MULTI_ARCH_CPU == X64
#		if HT_PLATFORM_MULTI_ARCH_COMPILER == std
static const char* g_ArchSufficesTable[] =
{
	"",
	"_SSE3",
	"_SSE4",
	"_AVX",
	"_AVX2"
};

enum HT_Architectures HT_DetectMicroArchitecture()
{
	// Check if cpuid instruction is supported;
	// If not, use NONE architecture.
	if (cpuid_present() == 0)
	{
		fprintf(stderr, "Failed to detect CPU architecture: cpuid is not supported by your CPU.\n");
		return HT_ARCHITECTURE_NONE;
	}

	struct cpu_raw_data_t raw;
	if (cpuid_get_raw_data(&raw) < 0)
	{
		fprintf(stderr, "Failed to detect CPU architecture: %s.\n", cpuid_error());
		return HT_ARCHITECTURE_NONE;
	}

	struct cpu_id_t data;
	if (cpu_identify(&raw, &data) < 0)
	{
		fprintf(stderr, "Failed to detect CPU architecture: %s.\n", cpuid_error());
		return HT_ARCHITECTURE_NONE;
	}

	enum HT_Architectures arch;

	if (data.flags[CPU_FEATURE_MMX] &&
		data.flags[CPU_FEATURE_SSE] &&
		data.flags[CPU_FEATURE_SSE2] &&
		data.flags[CPU_FEATURE_PNI] &&
		data.flags[CPU_FEATURE_SSSE3])
	{
		if (data.flags[CPU_FEATURE_SSE4_1] &&
			data.flags[CPU_FEATURE_SSE4_2])
		{
			if (data.flags[CPU_FEATURE_AVX] &&
				data.flags[CPU_FEATURE_AES] &&
				data.flags[CPU_FEATURE_PCLMUL])
			{
				if (data.flags[CPU_FEATURE_AVX2])
				{
					printf("Detected AVX2 architecture...");
					return HT_ARCHITECTURE_AVX2;
				}
				else
				{
					printf("Detected AVX architecture...");
					return HT_ARCHITECTURE_AVX;
				}
			}
			else
			{
				printf("Detected SSE4 architecture...");
				return HT_ARCHITECTURE_SSE4;
			}
		}
		else
		{
			printf("Detected SSE3 architecture...");
			return HT_ARCHITECTURE_SSE3;
		}
	}
	else
	{
		printf("Detected standard architecture...");
		return HT_ARCHITECTURE_NONE;
	}
}

#		elif HT_PLATFORM_MULTI_ARCH_COMPILER == msvc
static const char* g_ArchSufficesTable[] =
{
	"",
	"_SSE4",
	"_AVX",
	"_AVX2"
};

enum HT_Architectures HT_DetectMicroArchitecture()
{
	// Check if cpuid instruction is supported;
	// If not, use NONE architecture.
	if (cpuid_present() == 0)
	{
		fprintf(stderr, "Failed to detect CPU architecture: cpuid is not supported by your CPU.\n");
		return HT_ARCHITECTURE_NONE;
	}

	struct cpu_raw_data_t raw;
	if (cpuid_get_raw_data(&raw) < 0)
	{
		fprintf(stderr, "Failed to detect CPU architecture: %s.\n", cpuid_error());
		return HT_ARCHITECTURE_NONE;
	}

	struct cpu_id_t data;
	if (cpu_identify(&raw, &data) < 0)
	{
		fprintf(stderr, "Failed to detect CPU architecture: %s.\n", cpuid_error());
		return HT_ARCHITECTURE_NONE;
	}

	enum HT_Architectures arch;

	if (data.flags[CPU_FEATURE_MMX] &&
		data.flags[CPU_FEATURE_SSE] &&
		data.flags[CPU_FEATURE_SSE2])
	{
		if (data.flags[CPU_FEATURE_PNI] &&
			data.flags[CPU_FEATURE_SSSE3] &&
			data.flags[CPU_FEATURE_SSE4_1] &&
			data.flags[CPU_FEATURE_SSE4_2])
		{
			if (data.flags[CPU_FEATURE_AVX] &&
				data.flags[CPU_FEATURE_AES] &&
				data.flags[CPU_FEATURE_PCLMUL])
			{
				if (data.flags[CPU_FEATURE_AVX2])
				{
					printf("Detected AVX2 architecture...");
					return HT_ARCHITECTURE_AVX2;
				}
				else
				{
					printf("Detected AVX architecture...");
					return HT_ARCHITECTURE_AVX;
				}
			}
			else
			{
				printf("Detected SSE4 architecture...");
				return HT_ARCHITECTURE_SSE4;
			}
		}
		else
		{
			printf("Detected SSE2 architecture (none)...");
			return HT_ARCHITECTURE_NONE;
		}
	}
	else
	{
		printf("Detected standard architecture (FATAL! MSVC does not support this!)...");
		return HT_ARCHITECTURE_NONE;
	}
}

#		endif // HT_PLATFORM_MULTI_ARCH_COMPILER ==

#	elif HT_PLATFORM_MULTI_ARCH_CPU == X86 &&\
	  (HT_PLATFORM_MULTI_ARCH_COMPILER == msvc ||\
	  HT_PLATFORM_MULTI_ARCH_COMPILER == std)

static const char* g_ArchSufficesTable[] =
{
	"",
	"_SSE2"
};

enum HT_Architectures HT_DetectMicroArchitecture()
{
	// Check if cpuid instruction is supported;
	// If not, use NONE architecture.
	if (cpuid_present() == 0)
	{
		fprintf(stderr, "Failed to detect CPU architecture: cpuid is not supported by your CPU.\n");
		return HT_ARCHITECTURE_NONE;
	}

	struct cpu_raw_data_t raw;
	if (cpuid_get_raw_data(&raw) < 0)
	{
		fprintf(stderr, "Failed to detect CPU architecture: %s.\n", cpuid_error());
		return HT_ARCHITECTURE_NONE;
	}

	struct cpu_id_t data;
	if (cpu_identify(&raw, &data) < 0)
	{
		fprintf(stderr, "Failed to detect CPU architecture: %s.\n", cpuid_error());
		return HT_ARCHITECTURE_NONE;
	}

	if (data.flags[CPU_FEATURE_MMX] &&
		data.flags[CPU_FEATURE_SSE] &&
		data.flags[CPU_FEATURE_SSE2])
	{
		printf("Detected SSE2 architecture...");
		return HT_ARCHITECTURE_SSE2;
	}
	else
	{
		printf("Detected standard architecture...");
		return HT_ARCHITECTURE_NONE;
	}
}

#	endif // HT_PLATFORM_MULTI_ARCH_CPU ==

const char* HT_GetMicroArchitectureSuffix(enum HT_Architectures arch)
{
	return g_ArchSufficesTable[(unsigned)arch];
}

#else // HT_MULTI_ARCHITECTURES

enum HT_Architectures HT_DetectMicroArchitecture()
{
	return HT_ARCHITECTURE_NONE;
}

const char* HT_GetMicroArchitectureSuffix(enum HT_Architectures arch)
{
	return "";
}

#endif // HT_MULTI_ARCHITECTURES
