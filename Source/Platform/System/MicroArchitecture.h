/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SYSTEM_MICRO_ARCHITECTURE_H
#define SYSTEM_MICRO_ARCHITECTURE_H

#include "System/CpuConfig.h"

#ifdef HT_PLATFORM_MULTI_ARCH
#	if HT_PLATFORM_MULTI_ARCH_CPU == X64
#		if HT_PLATFORM_MULTI_ARCH_COMPILER == std
			enum HT_Architectures
			{
				HT_ARCHITECTURE_NONE,
				HT_ARCHITECTURE_SSE3,
				HT_ARCHITECTURE_SSE4,
				HT_ARCHITECTURE_AVX,
				HT_ARCHITECTURE_AVX2
			};
#		elif HT_PLATFORM_MULTI_ARCH_COMPILER == msvc
			enum HT_Architectures
			{
				HT_ARCHITECTURE_NONE,
				HT_ARCHITECTURE_SSE4,
				HT_ARCHITECTURE_AVX,
				HT_ARCHITECTURE_AVX2
			};
#		else // HT_PLATFORM_MULTI_ARCH_COMPILER ==
#			error Unupported compiler while multi-architecture turned on.
#		endif // HT_PLATFORM_MULTI_ARCH_COMPILER ==
#	elif HT_PLATFORM_MULTI_ARCH_CPU == X86
		enum HT_Architectures
		{
			HT_ARCHITECTURE_NONE,
			HT_ARCHITECTURE_SSE2
		};
#	else // HT_PLATFORM_MULTI_ARCH_CPU ==
#		error Unupported compiler while multi-architecture turned on.
#	endif // HT_PLATFORM_MULTI_ARCH_CPU ==
#else // HT_PLATFORM_MULTI_ARCH
	enum HT_Architectures { HT_ARCHITECTURE_NONE };
#endif // HT_PLATFORM_MULTI_ARCH

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

enum HT_Architectures HT_DetectMicroArchitecture();
const char* HT_GetMicroArchitectureSuffix(enum HT_Architectures arch);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // SYSTEM_MICRO_ARCHITECTURE_H
