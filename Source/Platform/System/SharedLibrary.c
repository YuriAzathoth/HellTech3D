/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include "FileSystem.h"
#include "System/FileSystem.h"
#include "SharedLibrary.h"

static void FinishSharedLibrarySuffix(char* dstptr,
									  const char* arch_suffix,
									  unsigned arch_suffix_size)
{
#ifdef HT_PLATFORM_MULTI_ARCH
	char* suffix_ptr = dstptr;

	if (arch_suffix_size > 0)
	{
		memcpy(dstptr, arch_suffix, arch_suffix_size);
		dstptr += arch_suffix_size;
	}
#endif // HT_PLATFORM_MULTI_ARCH

#ifndef NDEBUG
	dstptr[0] = '_';
	dstptr[1] = 'd';
	dstptr += 2;
#endif // NDEBUG

	dstptr[0] = '.';
#if defined(__WIN32__)
	dstptr[1] = 'd';
	dstptr[2] = 'l';
	dstptr[3] = 'l';
	dstptr += 4;
#else // defined
	dstptr[1] = 's';
	dstptr[2] = 'o';
	dstptr += 3;
#endif // defined
	dstptr[0] = '\0';
}

char* HT_SetSharedLibraryFullFilename(char* dst,
									  unsigned dst_cap_left,
									  const char* filepath,
									  unsigned filepath_size,
									  const char* filename,
									  unsigned filename_size,
									  const char* arch_suffix,
									  unsigned arch_suffix_size,
									  unsigned flags)
{
	// + '\0' at end.
	unsigned size_needed = filepath_size + filename_size + 1;

#ifndef _MSC_VER
	size_needed += 3; // 'lib' prefix.
#endif // _MSC_VER

#ifndef NDEBUG
	size_needed += 2; // '_d' debug suffix.
#endif // NDEBUG

#if defined(__WIN32__)
	size_needed += 4; // '.dll' extension.
#else // defined
	size_needed += 3; // '.so' extension.
#endif // defined

#ifdef HT_PLATFORM_MULTI_ARCH
	size_needed += arch_suffix_size;
#endif // HT_PLATFORM_MULTI_ARCH

	if (size_needed > dst_cap_left)
		return NULL;

	char* dstptr = dst;

	memcpy(dstptr, filepath, filepath_size);
	dstptr += filepath_size;

#ifndef _MSC_VER
	dstptr[0] = 'l';
	dstptr[1] = 'i';
	dstptr[2] = 'b';
	dstptr += 3;
#endif // _MSC_VER

	memcpy(dstptr, filename, filename_size);
	dstptr += filename_size;

#ifdef HT_PLATFORM_MULTI_ARCH
	char* suffix_ptr = dstptr;

	FinishSharedLibrarySuffix(dstptr, arch_suffix, arch_suffix_size);

	if (HT_IsFileExists(dst))
		return dstptr;
	else if ((flags & HT_SHARED_LIBRARY_FORCE_BRANCH) == 0) // Try NONE branch...
	{
		dstptr = suffix_ptr;
		FinishSharedLibrarySuffix(dstptr, "", 0);
		return HT_IsFileExists(dst) ? dstptr : NULL;
	}
	else
		return NULL;
#else // HT_PLATFORM_MULTI_ARCH
	FinishSharedLibrarySuffix(dstptr, "", 0);
	return HT_IsFileExists(dst) ? dstptr : NULL;
#endif // HT_PLATFORM_MULTI_ARCH
}

#ifdef __WIN32__
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

HT_SharedLibrary HT_LoadSharedLibrary(const char* full_filename)
{
	return (void*)LoadLibrary((LPCSTR)full_filename);
}

void HT_CloseSharedLibrary(HT_SharedLibrary lib)
{
	FreeLibrary((HINSTANCE)lib);
}

void* HT_GetSharedLibrarySymbol(HT_SharedLibrary lib, const char* symbol_name)
{
	return GetProcAddress((HINSTANCE)lib, (LPCSTR)symbol_name);
}

void HT_GetSysError(char* dst, unsigned dst_capacity)
{
	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		dst,
		dst_capacity,
		NULL
	);
}

#else // __WIN32__

#include <dlfcn.h>

HT_SharedLibrary HT_LoadSharedLibrary(const char* full_filename)
{
	return dlopen(full_filename, RTLD_LAZY);
}

void HT_CloseSharedLibrary(HT_SharedLibrary lib)
{
	dlclose(lib);
}

void* HT_GetSharedLibrarySymbol(HT_SharedLibrary lib, const char* symbol_name)
{
	return dlsym(lib, symbol_name);
}

void HT_GetSysError(char* dst, unsigned)
{
	strcpy(dst, dlerror());
}

#endif // __WIN32__
