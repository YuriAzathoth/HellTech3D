/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SYSTEM_SHARED_LIBRARY_H
#define SYSTEM_SHARED_LIBRARY_H

typedef void* HT_SharedLibrary;

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#define HT_SHARED_LIBRARY_FORCE_BRANCH 0x1

char* HT_SetSharedLibraryFullFilename(char* dst,
									  unsigned dst_cap_left,
									  const char* filepath,
									  unsigned filepath_size,
									  const char* filename,
									  unsigned filename_size,
									  const char* arch_suffix,
									  unsigned arch_suffix_size,
									  unsigned flags);

HT_SharedLibrary HT_LoadSharedLibrary(const char* full_filename);
void HT_CloseSharedLibrary(HT_SharedLibrary lib);
void* HT_GetSharedLibrarySymbol(HT_SharedLibrary lib, const char* symbol_name);
void HT_GetSysError(char* dst, unsigned dst_capacity);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // SYSTEM_SHARED_LIBRARY_H
