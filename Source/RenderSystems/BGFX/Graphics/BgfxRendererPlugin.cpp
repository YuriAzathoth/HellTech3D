/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <bgfx/bgfx.h>
#include "BgfxRendererPlugin.h"
#include "Graphics/GraphicsConfig.h"

namespace HT
{
static bgfx::RendererType::Enum GetRendererTypeToBgfx(RendererType type);
static uint32_t ParseRendererFlags(const RendererConfig& config);

BgfxRendererPlugin::BgfxRendererPlugin() :
	initialized_(false)
{
}

BgfxRendererPlugin::BgfxRendererPlugin(BgfxRendererPlugin&& other) noexcept :
	config_(other.config_),
	initialized_(other.initialized_)
{
	other.initialized_ = false;
}

void BgfxRendererPlugin::operator=(BgfxRendererPlugin&& other) noexcept
{
	Shutdown();

	config_ = other.config_;
	initialized_ = other.initialized_;
	other.initialized_ = false;
}

BgfxRendererPlugin::~BgfxRendererPlugin()
{
	Shutdown();
}

bool BgfxRendererPlugin::IsGpuSelectible() const
{
	return true;
}

bool BgfxRendererPlugin::SelectBestGpu(GPU& gpu) const
{
	bgfx::Init init{};
	init.type = bgfx::RendererType::Noop;
	if (!bgfx::init(init))
		return false;

	const bgfx::Caps* caps = bgfx::getCaps();
	if (caps == nullptr)
		goto error;

	if (caps->numGPUs < 1)
		goto error;

	uint16_t curr_vendor_id;
	for (unsigned i = 0; i < caps->numGPUs; ++i)
	{
		curr_vendor_id = caps->gpu[i].vendorId;
		if (curr_vendor_id != BGFX_PCI_ID_NONE)
		{
			gpu.device_id = caps->gpu[i].deviceId;
			gpu.vendor_id = curr_vendor_id;
			if (gpu.vendor_id == BGFX_PCI_ID_NVIDIA || gpu.vendor_id == BGFX_PCI_ID_AMD)
				break;
		}
	}

	bgfx::shutdown();
	return true;

error:
	bgfx::shutdown();
	return false;
}

bool BgfxRendererPlugin::Initialize(const RendererConfig& config, const WindowPlatformData& main_wpd)
{
	assert(config.resolution.width > 0);
	assert(config.resolution.height > 0);
	assert(main_wpd.window != nullptr);
	
	const uint32_t flags = ParseRendererFlags(config);

	bgfx::Init bgfx_info{};
	bgfx_info.type = GetRendererTypeToBgfx(config.renderer);
	bgfx_info.resolution.width = config.resolution.width;
	bgfx_info.resolution.height = config.resolution.height;
	bgfx_info.resolution.reset = flags;
	bgfx_info.deviceId = config.gpu.device_id;
	bgfx_info.vendorId = config.gpu.vendor_id;
	bgfx_info.debug = BGFX_DEBUG_NONE;
	bgfx_info.platformData.ndt = main_wpd.display;
	bgfx_info.platformData.nwh = main_wpd.window;

	if (!bgfx::init(bgfx_info))
		return false;

	config_ = config;
	initialized_ = true;

	return true;
}

void BgfxRendererPlugin::Reconfigure(const RendererConfig& config)
{
	if (initialized_ == false)
		return;

	const uint32_t flags = ParseRendererFlags(config);
	bgfx::reset(config.resolution.width, config.resolution.height, flags);
	config_ = config;
}

void BgfxRendererPlugin::Shutdown()
{
	if (initialized_ == true)
	{
		bgfx::shutdown();
		initialized_ = false;
	}
}

bool BgfxRendererPlugin::RenderFrame(RenderQueue& queue)
{
	bgfx::setViewRect(0, 0, 0, config_.resolution.width, config_.resolution.height);
	bgfx::touch(0);

	bgfx::frame();
}

static bgfx::RendererType::Enum GetRendererTypeToBgfx(RendererType type)
{
	switch (type)
	{
	case RendererType::Auto:		return bgfx::RendererType::Count;
	case RendererType::OpenGL:		return bgfx::RendererType::OpenGL;
	case RendererType::Vulkan:		return bgfx::RendererType::Vulkan;
#if defined(__WIN32__)
	case RendererType::Direct3D11:	return bgfx::RendererType::Direct3D11;
	case RendererType::Direct3D12:	return bgfx::RendererType::Direct3D12;
#elif defined(OSX)
	case RendererType::Metal:		return bgfx::RendererType::Metal;
#endif // defined
	case RendererType::None:		return bgfx::RendererType::Noop;
	default:						return bgfx::RendererType::Count;
	}
}

static uint32_t ParseRendererFlags(const RendererConfig& config)
{
	unsigned flags = BGFX_RESET_HIDPI;
	if (config.window_mode == WindowMode::FULLSCREEN || config.window_mode == WindowMode::BORDERLESS)
		flags |= BGFX_RESET_FULLSCREEN;
	if (config.vsync == true)
		flags |= BGFX_RESET_VSYNC;
	switch (config.msaa)
	{
	case ExponentialLevel::X2:
		flags |= BGFX_RESET_MSAA_X2;
		break;
	case ExponentialLevel::X4:
		flags |= BGFX_RESET_MSAA_X4;
		break;
	case ExponentialLevel::X8:
		flags |= BGFX_RESET_MSAA_X8;
		break;
	case ExponentialLevel::X16:
		flags |= BGFX_RESET_MSAA_X16;
		break;
	default:; // No flags.
	}
	return flags;
}
} // namespace HT
