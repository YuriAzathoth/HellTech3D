/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GRAPHICS_BGFX_RENDERER_PLUGIN_H
#define GRAPHICS_BGFX_RENDERER_PLUGIN_H

#include "Graphics/GraphicsConfig.h"
#include "Graphics/RendererPlugin.h"

namespace HT
{
class BgfxRendererPlugin : public RendererPlugin
{
public:
	BgfxRendererPlugin();
	BgfxRendererPlugin(BgfxRendererPlugin&& other) noexcept;
	void operator=(BgfxRendererPlugin&& other) noexcept;
	~BgfxRendererPlugin();

	bool IsGpuSelectible() const override;
	bool SelectBestGpu(GPU& gpu) const override;

	bool Initialize(const RendererConfig& config, const WindowPlatformData& main_wpd) override;
	void Reconfigure(const RendererConfig& config) override;
	void Shutdown() override;

	bool RenderFrame(RenderQueue& queue) override;

private:
	RendererConfig config_;
	bool initialized_;
};
} // namespace HT

#endif // GRAPHICS_BGFX_RENDERER_PLUGIN_H
