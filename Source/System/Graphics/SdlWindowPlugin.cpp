/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <utility>
#include "Graphics/GraphicsConfig.h"
#include "SdlWindowPlugin.h"

namespace HT
{
SdlWindowPlugin::SdlWindowPlugin()
{
}

SdlWindowPlugin::~SdlWindowPlugin()
{
	Destroy();
	Shutdown();
}

SdlWindowPlugin::SdlWindowPlugin(SdlWindowPlugin&& other) noexcept :
	window_(std::move(other.window_))
{
}

void SdlWindowPlugin::operator=(SdlWindowPlugin&& other) noexcept
{
	Destroy();

	window_ = std::move(other.window_);
}

bool SdlWindowPlugin::Initialize()
{
	return window_.InitVideoSubsystem();
}

void SdlWindowPlugin::Shutdown()
{
	window_.ShutdownVideoSubsystem();
}

bool SdlWindowPlugin::Create(const WindowConfig& init_config)
{
	if (init_config.resolution.width == 0 || init_config.resolution.height == 0)
	{
		if (!window_.GetMaxResolution(const_cast<Resolution&>(init_config.resolution), 0))
			return false;
	}

	return window_.Create(
		init_config.title,
		init_config.resolution,
		init_config.window_mode);
}

bool SdlWindowPlugin::Reconfigure(const WindowConfig& new_config)
{
	// TODO: Implements this.
	return false;
}

void SdlWindowPlugin::Destroy()
{
	if (window_.IsCreated())
		window_.Destroy();
}

bool SdlWindowPlugin::GetPlatformData(WindowPlatformData& wpd)
{
	return window_.GetPlatformData(wpd);
}
} // namespace HT
