/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GRAPHICS_SDL_WINDOW_PLUGIN_H
#define GRAPHICS_SDL_WINDOW_PLUGIN_H

#include "Graphics/WindowPlugin.h"
#include "HellTech3DSystemAPI.h"
#include "SystemWindow.h"

namespace HT
{
class HELLTECH3DSYSTEMAPI_EXPORT SdlWindowPlugin : public WindowPlugin
{
public:
	SdlWindowPlugin();
	~SdlWindowPlugin();

	SdlWindowPlugin(SdlWindowPlugin&& other) noexcept;
	void operator=(SdlWindowPlugin&& other) noexcept;

	bool Initialize() override;
	void Shutdown() override;

	bool Create(const WindowConfig& init_config) override;
	bool Reconfigure(const WindowConfig& new_config) override;
	void Destroy() override;

	bool GetPlatformData(WindowPlatformData& wpd) override;

private:
	SystemWindow window_;
};
} // namespace HT

#endif // GRAPHICS_SDL_WINDOW_PLUGIN_H
