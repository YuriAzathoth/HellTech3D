/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#include "SystemWindow.h"
#include "Graphics/GraphicsConfig.h"
#include "SDL_video.h"

namespace HT
{
static bool s_video_initialized = false;

static bool GetDisplayMode(SDL_DisplayMode& sdl_mode, const Resolution& config_res, uint16_t display_id);
static bool GetDisplayModeMaxRes(SDL_DisplayMode& sdl_mode, uint16_t display_id);
static uint32_t ParseFlags(WindowMode mode);

SystemWindow::SystemWindow() :
	sdl_window_(nullptr)
{
}

SystemWindow::SystemWindow(SystemWindow&& other) noexcept :
	sdl_window_(other.sdl_window_)
{
	other.sdl_window_ = nullptr;
}

void SystemWindow::operator=(SystemWindow&& other) noexcept
{
	if (IsCreated())
		Destroy();

	sdl_window_ = other.sdl_window_;
	other.sdl_window_ = nullptr;
}

SystemWindow::~SystemWindow()
{
	if (IsCreated())
		Destroy();
}

bool SystemWindow::InitVideoSubsystem()
{
	if (s_video_initialized == false)
		s_video_initialized = (SDL_InitSubSystem(SDL_INIT_VIDEO) == 0);
	return s_video_initialized;
}

void SystemWindow::ShutdownVideoSubsystem()
{
	if (s_video_initialized == true)
		SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

bool SystemWindow::Create(const char* title, const Resolution& resolution, WindowMode mode)
{
	uint32_t flags = SDL_WINDOW_ALLOW_HIGHDPI;
	
	switch (mode)
	{
	case WindowMode::WINDOWED:
		// No additional flags.
		break;
	case WindowMode::BORDERLESS:
		flags |= SDL_WINDOW_BORDERLESS;
		[[fallthrough]]; // Also has next flag.
	case WindowMode::FULLSCREEN:
		flags |= SDL_WINDOW_FULLSCREEN;
		break;
	}

	sdl_window_ = SDL_CreateWindow(title,
								   SDL_WINDOWPOS_CENTERED,
								   SDL_WINDOWPOS_CENTERED,
								   static_cast<int>(resolution.width),
								   static_cast<int>(resolution.height),
								   flags);
	if (sdl_window_ == nullptr)
		return false;

	return SetResolution(resolution);
}

void SystemWindow::Destroy()
{
	SDL_DestroyWindow(sdl_window_);
	sdl_window_ = nullptr;
}

bool SystemWindow::GetPlatformData(WindowPlatformData& wpd) const
{
#if !BX_PLATFORM_EMSCRIPTEN
	SDL_SysWMinfo wmi;
	SDL_VERSION(&wmi.version);
	if (!SDL_GetWindowWMInfo(sdl_window_, &wmi))
		return false;
#endif // !BX_PLATFORM_EMSCRIPTEN

#if SDL_VIDEO_DRIVER_WINDOWS
	wpd.display = nullptr;
	wpd.window = wmi.info.win.window;
#elif SDL_VIDEO_DRIVER_WINRT
	wpd.display = nullptr;
	wpd.window = wmi.info.winrt.window;
#elif SDL_VIDEO_DRIVER_X11
	wpd.display = wmi.info.x11.display;
	wpd.window = reinterpret_cast<void*>(wmi.info.x11.window);
#elif SDL_VIDEO_DRIVER_COCOA
	wpd.display = nullptr;
	wpd.window = wmi.info.cocoa.window;
#elif SDL_VIDEO_DRIVER_UIKIT
	wpd.display = nullptr;
	wpd.window = wmi.info.uikit.window;
#elif SDL_VIDEO_DRIVER_WAYLAND
	wpd.display = wmi.info.wl.display;
	wpd.window = wmi.info.wl.egl_window;
#elif SDL_VIDEO_DRIVER_WAYLAND
	wpd.display = wmi.info.wl.display;
	wpd.window = wmi.info.wl.egl_window;
#elif SDL_VIDEO_DRIVER_ANDROID
	wpd.display = nullptr;
	wpd.window = wmi.info.android.window;
#elif SDL_VIDEO_DRIVER_EMSCRIPTEN
	wpd.display = nullptr;
	wpd.window = (void*)"#canvas";
#endif // BX_PLATFORM_

	return true;
}

uint32_t SystemWindow::GetNativeId() const
{
	return SDL_GetWindowID(sdl_window_);
}

void SystemWindow::SetMouseGrab(bool grab)
{
	const SDL_bool grab_num = grab ? SDL_TRUE : SDL_FALSE;
	if (SDL_SetRelativeMouseMode(grab_num) < 0)
		SDL_SetWindowGrab(sdl_window_, grab_num);
}

void SystemWindow::SetResizeable(bool resizeable)
{
	SDL_SetWindowResizable(sdl_window_, resizeable ? SDL_TRUE : SDL_FALSE);
}

bool SystemWindow::GetResolutionsList(std::vector<Resolution>& dst, uint16_t display_id)
{
	const int modes_num = SDL_GetNumDisplayModes(static_cast<int>(display_id));
	if (modes_num < 1)
		return false;

	dst.clear();
	dst.reserve(static_cast<size_t>(modes_num));

	Resolution resolution;
	SDL_DisplayMode sdl_mode;
	for (int mode_id = 0; mode_id < modes_num; ++mode_id)
	{
		if (SDL_GetDisplayMode(static_cast<int>(display_id), mode_id, &sdl_mode) < 0)
		{
			dst.clear();
			return false;
		}

		resolution.width = sdl_mode.w;
		resolution.height = sdl_mode.h;
		resolution.refresh_rate = sdl_mode.refresh_rate;
		dst.push_back(resolution);
	}

	return true;
}

bool SystemWindow::GetMaxResolution(Resolution& dst, uint16_t display_id)
{
	const int modes_num = SDL_GetNumDisplayModes(static_cast<int>(display_id));
	if (modes_num < 1)
		return false;

	SDL_DisplayMode sdl_mode;
	if (SDL_GetDisplayMode(static_cast<int>(display_id), 0, &sdl_mode) < 0)
		return false;

	dst.width = sdl_mode.w;
	dst.height = sdl_mode.h;
	dst.refresh_rate = sdl_mode.refresh_rate;

	return true;
}

bool SystemWindow::SetResolution(const Resolution& resolution)
{
	if (GetWindowMode() == WindowMode::FULLSCREEN)
	{
		const int display_id = SDL_GetWindowDisplayIndex(sdl_window_);
		const int modes_num = SDL_GetNumDisplayModes(display_id);
		if (modes_num < 1)
			return false;

		SDL_DisplayMode sdl_mode;
		for (int mode_id = 0; mode_id < modes_num; ++mode_id)
		{
			if (SDL_GetDisplayMode(display_id, mode_id, &sdl_mode) < 0)
				return false;
			
			if (sdl_mode.w == resolution.width &&
				sdl_mode.h == resolution.height &&
				sdl_mode.refresh_rate == resolution.refresh_rate)
			{
				return SDL_SetWindowDisplayMode(sdl_window_, &sdl_mode) == 0;
			}
		}
		return false;
	}
	else
	{
		SDL_SetWindowSize(sdl_window_, resolution.width, resolution.height);
		return true;
	}
}

bool SystemWindow::GetResolution(Resolution& resolution) const
{
	SDL_DisplayMode sdl_mode;
	if (SDL_GetWindowDisplayMode(sdl_window_, &sdl_mode) < 0)
		return false;

	if (GetWindowMode() == WindowMode::FULLSCREEN)
	{
		resolution.width = sdl_mode.w;
		resolution.height = sdl_mode.h;
		resolution.refresh_rate = sdl_mode.refresh_rate;
	}
	else
	{
		int width, height;
		SDL_GetWindowSize(sdl_window_, &width, &height);
		resolution.width = static_cast<uint16_t>(width);
		resolution.height = static_cast<uint16_t>(height);
	}
	
	return true;
}

void SystemWindow::SetTitle(const char* title)
{
	SDL_SetWindowTitle(sdl_window_, title);
}

const char* SystemWindow::GetTitle() const
{
	return SDL_GetWindowTitle(sdl_window_);
}

bool SystemWindow::SetWindowMode(WindowMode mode)
{
	const SDL_bool BORDERED_TABLE[] = { SDL_TRUE, SDL_FALSE, SDL_FALSE };
	const SDL_bool FULLSCREEN_TABLE[] = { SDL_TRUE, SDL_TRUE, SDL_FALSE };
	SDL_SetWindowBordered(sdl_window_, BORDERED_TABLE[static_cast<int>(mode)]);
	return SDL_SetWindowFullscreen(sdl_window_, FULLSCREEN_TABLE[static_cast<int>(mode)]) == 0;
}

WindowMode SystemWindow::GetWindowMode() const
{
	const uint32_t flags = SDL_GetWindowFlags(sdl_window_);
	if ((flags & SDL_WINDOW_FULLSCREEN) == 0)
		return WindowMode::WINDOWED;
	else if ((flags & SDL_WINDOW_BORDERLESS) != 0)
		return WindowMode::BORDERLESS;
	else
		return WindowMode::FULLSCREEN;
}
} // namespace HT
