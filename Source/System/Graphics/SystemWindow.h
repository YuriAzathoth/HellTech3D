/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GRAPHICS_SYSTEM_WINDOW_H
#define GRAPHICS_SYSTEM_WINDOW_H

#include <stdint.h>
#include <vector>
#include "Graphics/GraphicsConfig.h"

typedef struct SDL_Window SDL_Window;

namespace HT
{
class SystemWindow
{
public:
	SystemWindow();
	SystemWindow(SystemWindow&& other) noexcept;
	void operator=(SystemWindow&& other) noexcept;
	~SystemWindow();

	bool Create(const char* title, const Resolution& resolution, WindowMode mode);
	void Destroy();

	void SetMouseGrab(bool grab);
	void SetResizeable(bool resizeable);
	bool SetResolution(const Resolution& resolution);
	void SetTitle(const char* title);
	bool SetWindowMode(WindowMode mode);

	bool GetPlatformData(WindowPlatformData& wpd) const;
	uint32_t GetNativeId() const;
	bool GetResolution(Resolution& resolution) const;
	const char* GetTitle() const;
	WindowMode GetWindowMode() const;

	bool IsCreated() const { return sdl_window_ != nullptr; }

private:
	SDL_Window* sdl_window_;

	SystemWindow(const SystemWindow&) = delete;
	void operator=(const SystemWindow&) = delete;

public:
	static bool InitVideoSubsystem();
	static void ShutdownVideoSubsystem();
	
	static bool GetResolutionsList(std::vector<Resolution>& dst, uint16_t display_id);
	static bool GetMaxResolution(Resolution& dst, uint16_t display_id);
};
} // namespace HT

#endif // GRAPHICS_SYSTEM_WINDOW_H
