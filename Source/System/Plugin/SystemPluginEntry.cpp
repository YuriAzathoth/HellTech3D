/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Graphics/SdlWindowPlugin.h"
#include "HellTech3DSystemAPI.h"

#define HT_PLUGIN_EXPORT HELLTECH3DSYSTEMAPI_EXPORT
#include "Plugin/PluginEntryDefine.h"

using namespace HT;

HT_PLUGIN_FACTORIES_BEGIN()
HT_PLUGIN_FACTORY(SdlWindowPlugin, PluginType::WINDOW)
HT_PLUGIN_FACTORIES_END()
