#
# HellTech3D - Game engine
# Copyright (C) 2024 Yuriy Zinchenko
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

SET (BGFX_BUILD_EXAMPLES OFF CACHE BOOL "")
SET (BGFX_OPENGL_VERSION 21 CACHE STRING "")
IF (EMSCRIPTEN)
	SET (BGFX_CONFIG_MULTITHREADED OFF CACHE BOOL "")
ENDIF ()
ADD_SUBDIRECTORY (BGFX)

SET (CGLM_SHARED OFF CACHE BOOL "")
SET (CGLM_STATIC ON CACHE BOOL "")
ADD_SUBDIRECTORY (CGLM)

SET (FLECS_SHARED ON CACHE BOOL "")
SET (FLECS_STATIC OFF CACHE BOOL "")
ADD_SUBDIRECTORY (FLECS)

IF (HT_MULTI_ARCHITECTURES)
	SET (LIBCPUID_BUILD_DEPRECATED OFF CACHE BOOL "")
	SET (LIBCPUID_BUILD_DRIVERS OFF CACHE BOOL "")
	SET (LIBCPUID_ENABLE_DOCS OFF CACHE BOOL "")
	SET (LIBCPUID_ENABLE_TESTS OFF CACHE BOOL "")
	ADD_SUBDIRECTORY (LibCpuid)
ENDIF ()

IF (NOT EMSCRIPTEN)
	SET (SDL2_DISABLE_SDL2MAIN ON CACHE BOOL "")

	SET (SDL_SHARED ON CACHE BOOL "")
	SET (SDL_STATIC OFF CACHE BOOL "")

	SET (SDL_CMAKE_DEBUG_POSTFIX "_d" CACHE STRING "")

	# Disable SIMD in SDL.
	# Else, for example, SDL plugin with NONE arch will be linked with
	# -march=native SDL library and will cause crashes on older CPUs.
	# TODO: Add architectures to be linked with each SDL plugin.
	SET (SDL_ASSEMBLY OFF CACHE BOOL "")
	SET (SDL_SSEMATH OFF CACHE BOOL "")
	SET (SDL_SSE OFF CACHE BOOL "")
	SET (SDL_SSE2 OFF CACHE BOOL "")
	SET (SDL_SSE3 OFF CACHE BOOL "")
	SET (SDL_MMX OFF CACHE BOOL "")
	SET (SDL_3DNOW OFF CACHE BOOL "")
	SET (SDL_ALTIVEC OFF CACHE BOOL "")
	SET (SDL_ARMSIMD OFF CACHE BOOL "")
	SET (SDL_ARMNEON OFF CACHE BOOL "")
	SET (SDL_LSX OFF CACHE BOOL "")
	SET (SDL_LASX OFF CACHE BOOL "")

	ADD_SUBDIRECTORY (SDL)
ENDIF ()

IF (HT_BUILD_TESTS)
	ADD_SUBDIRECTORY (Doctest)
ENDIF ()
