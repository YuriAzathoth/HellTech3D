/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINER_DEBUG_ALLOCATOR_H
#define CONTAINER_DEBUG_ALLOCATOR_H

#include <memory>
#include <unordered_map>

namespace HT
{
template <typename UnderlyingAllocator>
struct DebugAllocator
{
public:
	using ValueType = typename UnderlyingAllocator::ValueType;
	using Pointer = typename UnderlyingAllocator::Pointer;
	using ConstPointer = typename UnderlyingAllocator::ConstPointer;
	using SizeType = typename UnderlyingAllocator::SizeType;
	using DifferenceType = typename UnderlyingAllocator::DifferenceType;

	enum class ErrorType
	{
		NO_ERROR,
		ALLOCATED_BUT_NOT_FREED,
		FREED_BUT_NOT_ALLOCATED,
		FREED_WRONG_SIZE
	};

	DebugAllocator() noexcept :
		alloc_data_(std::make_shared<SharedData>())
	{
	}

	DebugAllocator(const DebugAllocator& other) noexcept :
		alloc_(other.alloc_),
		alloc_data_(other.alloc_data_)
	{
	}

	void operator=(const DebugAllocator& other) noexcept
	{
		alloc_ = other.alloc_;
		alloc_data_ = other.alloc_data_;
	}

	template <typename OtherAllocator>
	bool operator==(const OtherAllocator& other) const noexcept
	{
		return alloc_ == other.alloc_;
	}

	bool operator==(const DebugAllocator& other) const noexcept
	{
		return alloc_ == other.alloc_ && alloc_data_.get() == other.alloc_data_.get();
	}

	Pointer Allocate(SizeType count) noexcept
	{
		Pointer ptr = alloc_.Allocate(count);

		void* vptr = reinterpret_cast<void*>(ptr);
		alloc_data_->allocated_but_not_freed.emplace(vptr, count);

		return ptr;
	}

	void Deallocate(Pointer ptr, SizeType count) noexcept
	{
		void* vptr = reinterpret_cast<void*>(ptr);
		auto it = alloc_data_->allocated_but_not_freed.find(vptr);
		if (it != alloc_data_->allocated_but_not_freed.end())
		{
			if (it->second == count)
				alloc_.Deallocate(ptr, count);
			else
			{
				alloc_.Deallocate(ptr, it->second);

				WrongSizeData wsd;
				wsd.allocated = it->second;
				wsd.deallocated = count;
				alloc_data_->freed_wrong_size.emplace(vptr, wsd);
			}
			alloc_data_->allocated_but_not_freed.erase(it);
		}
		else
			alloc_data_->freed_not_allocated.emplace(vptr, count);
	}

	bool IsFine() const noexcept
	{
		return alloc_data_->allocated_but_not_freed.empty() &&
			   alloc_data_->freed_wrong_size.empty() &&
			   alloc_data_->freed_not_allocated.empty();
	}

	ErrorType GetError(void** ptr, SizeType& i1, SizeType& i2) noexcept
	{
		if (!alloc_data_->allocated_but_not_freed.empty())
		{
			auto it = alloc_data_->allocated_but_not_freed.begin();
			*ptr = it->first;
			i1 = it->second;
			alloc_data_->allocated_but_not_freed.erase(it);
			return ErrorType::ALLOCATED_BUT_NOT_FREED;
		}
		else if (!alloc_data_->freed_not_allocated.empty())
		{
			auto it = alloc_data_->freed_not_allocated.begin();
			*ptr = it->first;
			i1 = it->second;
			alloc_data_->freed_not_allocated.erase(it);
			return ErrorType::FREED_BUT_NOT_ALLOCATED;
		}
		else if (!alloc_data_->freed_wrong_size.empty())
		{
			auto it = alloc_data_->freed_wrong_size.begin();
			*ptr = it->first;
			i1 = it->second.allocated;
			i2 = it->second.deallocated;
			alloc_data_->freed_wrong_size.erase(it);
			return ErrorType::FREED_WRONG_SIZE;
		}
		else
			return ErrorType::NO_ERROR;
	}

private:
	struct WrongSizeData
	{
		SizeType allocated;
		SizeType deallocated;
	};

	struct SharedData
	{
		std::unordered_map<void*, SizeType> allocated_but_not_freed;
		std::unordered_map<void*, SizeType> freed_not_allocated;
		std::unordered_map<void*, WrongSizeData> freed_wrong_size;
	};

	UnderlyingAllocator alloc_;
	std::shared_ptr<SharedData> alloc_data_;
};
} // namespace HT

#endif // CONTAINER_DEBUG_ALLOCATOR_H
