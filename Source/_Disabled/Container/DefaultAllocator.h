/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINER_DEFAULT_ALLOCATOR_H
#define CONTAINER_DEFAULT_ALLOCATOR_H

#include <stdint.h>
#include <new>

namespace HT
{
template <typename T>
struct DefaultAllocator
{
public:
	using ValueType = T;
	using Pointer = T*;
	using ConstPointer = T*;
	using SizeType = size_t;
	using DifferenceType = ptrdiff_t;

	constexpr DefaultAllocator() noexcept {}
	constexpr DefaultAllocator(const DefaultAllocator& other) noexcept {}
	constexpr void operator=(const DefaultAllocator& other) noexcept {}
	constexpr bool operator==(DefaultAllocator) const noexcept { return true; }

	template <typename OtherAllocator>
	constexpr bool operator==(OtherAllocator) const noexcept { return false; }

	constexpr T* Allocate(SizeType count) const noexcept
	{
		return reinterpret_cast<T*>(::operator new(count * sizeof(T), std::nothrow));
	}

	constexpr void Deallocate(T* ptr, SizeType count) const noexcept
	{
		::operator delete(reinterpret_cast<void*>(ptr), std::nothrow);
	}
};
} // namespace HT

#endif // CONTAINER_DEFAULT_ALLOCATOR_H
