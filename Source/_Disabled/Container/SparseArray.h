/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTAINER_SPARSE_ARRAY_H
#define CONTAINER_SPARSE_ARRAY_H

#include <stdint.h>
#include <string.h>
#include <bit>
#include <limits>
#include <memory>
#include <type_traits>
#include "Container/DefaultAllocator.h"

namespace HT
{
template <typename Index,
		  typename Value,
		  typename Bitfield = uint32_t,
		  typename Allocator = DefaultAllocator<uint8_t>>
class SparseArray
{
public:
	using IndexType = Index;
	using ValueType = Value;
	using SizeType = IndexType;
	using Reference = ValueType&;
	using ConstReference = const ValueType&;
	using Pointer = ValueType*;
	using ConstPointer = const ValueType*;
	using AllocatorType = Allocator;
	using BitfieldType = Bitfield;

	static constexpr IndexType INVALID_KEY = std::numeric_limits<IndexType>::max();

	static_assert(std::is_integral_v<Index> && std::is_unsigned_v<Index>);
	static_assert(std::is_trivial_v<Value>);
	static_assert(std::is_integral_v<Bitfield> && std::is_unsigned_v<Bitfield>);

	SparseArray() noexcept :
		size_(0),
		capacity_(0)
	{
	}

	explicit SparseArray(const Allocator& alloc) noexcept :
		size_(0),
		capacity_(0),
		alloc_(alloc)
	{
	}

	SparseArray(const SparseArray& other) noexcept :
		size_(other.size_),
		capacity_(other.capacity_),
		alloc_(other.alloc_)
	{
		const SizeType bitfield_size = allocate(data_, items_state_, capacity_);
		memcpy(data_, other.data_, capacity_ * sizeof(ValueType));
		memcpy(items_state_, other.items_state_, bitfield_size);
	}

	SparseArray(SparseArray&& other) noexcept :
		data_(other.data_),
		items_state_(other.items_state_),
		size_(other.size_),
		capacity_(other.capacity_),
		alloc_(other.alloc_)
	{
		other.size_ = 0;
		other.capacity_ = 0;
	}

	~SparseArray() noexcept { Destroy(); }

	void operator=(const SparseArray& other) noexcept
	{
		Destroy();

		size_ = other.size_;
		capacity_ = other.capacity_;

		const SizeType bitfield_size = allocate(data_, items_state_, capacity_);
		memcpy(data_, other.data_, capacity_ * sizeof(ValueType));
		memcpy(items_state_, other.items_state_, bitfield_size);
	}

	void operator=(SparseArray&& other) noexcept
	{
		Destroy();

		data_ = other.data_;
		items_state_ = other.items_state_;
		size_ = other.size_;
		capacity_ = other.capacity_;

		other.size_ = 0;
		other.capacity_ = 0;
	}

	ValueType& operator[](IndexType key) noexcept { return data_[key]; }
	const ValueType& operator[](IndexType key) const noexcept { return data_[key]; }

	SizeType Capacity() const noexcept { return capacity_; }
	SizeType Size() const noexcept { return size_; }
	[[nodiscard]] bool Empty() const noexcept { return size_ == 0; }

	bool Contains(IndexType key) const noexcept noexcept
	{
		const SizeType segment = GetBfSegment(key);
		const BitfieldType bit = GetBfBit(key);
		return capacity_ > 0 && GetBfValue(items_state_[segment], bit);
	}

	IndexType Insert(ValueType value) noexcept
	{
		if (size_ + 1 > capacity_)
			if (!Reserve(capacity_ + BITS_IN_BITFIELD))
				return INVALID_KEY;

		++size_;

		const IndexType id = TakeEmptyCell();
		if (id != INVALID_KEY)
			data_[id] = value;

		return id;
	}

	void Erase(IndexType key) noexcept
	{
		const SizeType segment = GetBfSegment(key);
		const BitfieldType bit = GetBfBit(key);
		DisableBfBit(items_state_[segment], bit);

		--size_;
	}

	void Clear() noexcept
	{
		Destroy();
		size_ = 0;
		capacity_ = 0;
	}

	bool Reserve(SizeType count) noexcept
	{
		Pointer new_data;
		BitfieldType* new_items_state;
		const SizeType bitfield_count = Allocate(new_data, new_items_state, count);
		if (bitfield_count > 0)
		{
			const SizeType old_data_size = capacity_ * sizeof(ValueType);
			const SizeType old_items_state_size = GetBfSize(capacity_) * sizeof(BitfieldType);
			const SizeType new_data_size = count * sizeof(ValueType);
			const SizeType new_items_state_size = bitfield_count * sizeof(BitfieldType);

			if (capacity_ > 0)
			{
				memcpy(new_data, data_, old_data_size);
				memcpy(new_items_state, items_state_, old_items_state_size);
				Deallocate(data_, items_state_);
			}

			data_ = new_data;
			items_state_ = new_items_state;
			capacity_ = count;

			return true;
		}
		else
			return false;
	}

	bool ShrinkToFit() noexcept
	{
		SizeType to_deallocate = GetSegmentsToShrink();
		if (to_deallocate == 0)
			return false;

		const SizeType new_capacity = capacity_ - to_deallocate * BITS_IN_BITFIELD;
		if (new_capacity > 0)
		{
			Pointer new_data;
			BitfieldType* new_items_state;
			const SizeType new_items_state_size = Allocate(new_data, new_items_state, new_capacity);
			if (new_items_state_size == 0)
				return false;

			if (capacity_)
			{
				memcpy(new_data, data_, capacity_ * sizeof(ValueType));
				memcpy(new_items_state, items_state_, new_items_state_size);
				Deallocate(data_, items_state_);
			}

			data_ = new_data;
			items_state_ = new_items_state;
		}
		else
			Deallocate(data_, items_state_);

		capacity_ = new_capacity;

		return true;
	}

	size_t MemorySize() const noexcept
	{
		return sizeof(SparseArray) + capacity_ * sizeof(ValueType) + GetBfSize(capacity_) * sizeof(BitfieldType);
	}

/*#ifndef NDEBUG
	void debug_print() const
	{
		printf("Sparse Map\n");
		printf("  Size:     %u\n", size_);
		printf("  Capacity: %u\n", capacity_);
		for (IndexType i = 0; i < capacity_; ++i)
		{
			printf("    %u[%u, %u -> %u]:\t", i, get_bf_segment(i), get_bf_bit_position(i), get_bf_bit(i));
			if (contains(i))
				printf("%u\n", data_[i]);
			else
				printf("Empty\n");
		}
	}
#endif // NDEBUG*/

private:
	inline static constexpr BitfieldType BITS_IN_BITFIELD = static_cast<BitfieldType>(sizeof(BitfieldType) * 8); // 4 * 8 = 32
	inline static constexpr BitfieldType BITS_ONE = static_cast<BitfieldType>(1);
	inline static constexpr BitfieldType BITS_ALL_DISABLED = static_cast<BitfieldType>(0);
	inline static constexpr BitfieldType BITS_ALL_ENABLED = ~BITS_ALL_DISABLED;
	inline static constexpr IndexType BITFIELD_MAX_VALUE_BITS = std::bit_width(BITS_IN_BITFIELD - 1);

	SizeType Allocate(Pointer& data, BitfieldType*& items_state, SizeType capacity) noexcept
	{
		const SizeType data_size = capacity * sizeof(ValueType);
		const SizeType bitfield_size = GetBfSize(capacity) * sizeof(BitfieldType);
		uint8_t* memory = alloc_.allocate(data_size + bitfield_size);
		data = reinterpret_cast<Pointer>(memory);
		memory += data_size;
		items_state = reinterpret_cast<BitfieldType*>(memory);

		if (memory != nullptr)
		{
			memset(items_state, 0, bitfield_size);
			return bitfield_size;
		}
		else
			return 0;
	}

	void Deallocate(Pointer data, [[maybe_unused]] BitfieldType* items_state) noexcept
	{
		const SizeType size = capacity_ * sizeof(ValueType) + GetBfSize(capacity_) * sizeof(BitfieldType);
		alloc_.Deallocate(reinterpret_cast<uint8_t*>(data), size);
	}

	void Destroy() noexcept
	{
		if (capacity_ > 0)
			Deallocate(data_, items_state_);
	}

	IndexType TakeEmptyCell() noexcept
	{
		const SizeType bitfield_count = GetBfSize(capacity_);
		for (SizeType i = 0; i < bitfield_count; ++i)
		{
			if (!IsBfSegmentFull(items_state_[i]))
			{
				const BitfieldType bit = GetBfFirstEmptyBit(items_state_[i]);
				EnableBfBit(items_state_[i], bit);
				return BuildBfKey(i, bit);
			}
		}
		return INVALID_KEY;
	}

	SizeType GetSegmentsToShrink() const noexcept
	{
		SizeType ret = 0;
		SizeType segment = GetBfSize(capacity_) - 1;
		while (items_state_[segment] == BITS_ALL_DISABLED)
		{
			++ret;
			if (segment == 0)
				break;
			--segment;
		}
		return ret;
	}

	static constexpr SizeType GetBfSegment(IndexType key) noexcept
	{
		return static_cast<SizeType>(key >> BITFIELD_MAX_VALUE_BITS);
	}

	static constexpr BitfieldType GetBfBitPosition(IndexType key) noexcept
	{
		return static_cast<BitfieldType>(key) & (BITS_IN_BITFIELD - 1);
	}

	static constexpr BitfieldType GetBfBit(IndexType key) noexcept
	{
		return BITS_ONE << get_bf_bit_position(key);
	}

	static constexpr BitfieldType GetBfFirstEmptyBit(BitfieldType bitfield) noexcept
	{
		return get_bf_bit(std::countr_one(bitfield));
	}

	static constexpr bool GetBfValue(BitfieldType bitfield, BitfieldType bit) noexcept
	{
		return bitfield & bit;
	}

	static constexpr bool IsBfSegmentFull(BitfieldType bitfield) noexcept
	{
		return bitfield == BITS_ALL_ENABLED;
	}

	static constexpr void EnableBfBit(BitfieldType& bitfield, BitfieldType bit) noexcept
	{
		bitfield |= bit;
	}

	static constexpr void DisableBfBit(BitfieldType& bitfield, BitfieldType bit) noexcept
	{
		bitfield &= ~bit;
	}

	static constexpr SizeType BuildBfKey(SizeType segment, BitfieldType bit) noexcept
	{
		return (segment << BITFIELD_MAX_VALUE_BITS) | std::countr_zero(bit);
	}

	static constexpr SizeType GetBfSize(SizeType capacity) noexcept
	{
		return capacity >> BITFIELD_MAX_VALUE_BITS;
	}

	Pointer data_;
	BitfieldType* items_state_;
	SizeType size_;
	SizeType capacity_;
	AllocatorType alloc_;
};
} // namespace HT

#endif // CONTAINER_SPARSE_ARRAY_H
