/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <utility>
#include "Graphics.h"
#include "Graphics/RendererPlugin.h"

namespace HT
{
Graphics::Graphics()
{
	// TODO: Make renderers list by reading config.
	renderers_list_.resize(static_cast<size_t>(RendererType::None));
	for (unsigned i = 0; i < static_cast<size_t>(RendererType::None); ++i)
	{
		AvailableRenderer &record = renderers_list_[i];
		strcpy(record.filename, "HellTech3DPlugin_BGFX");
	}
}

Graphics::Graphics(Graphics&& other) noexcept :
	window_(std::move(other.window_)),
	renderer_plugin_(std::move(other.renderer_plugin_)),
	renderers_list_(std::move(other.renderers_list_))
{
}

void Graphics::operator=(Graphics&& other) noexcept
{
	window_ = std::move(other.window_);
	renderer_plugin_ = std::move(other.renderer_plugin_);
	renderers_list_ = std::move(other.renderers_list_);
}

Graphics::~Graphics()
{
	Shutdown();
}

bool Graphics::Initialize(WindowConfig& window_cfg, RendererConfig& renderer_cfg)
{
	if (!SystemWindow::InitVideoSubsystem())
		return false;

	if (!window_.Create(window_cfg.title, window_cfg.resolution, window_cfg.window_mode, 0))
		return false;

	WindowPlatformData wpd{};
	if (!window_.GetPlatformData(wpd))
	{
		window_.Destroy();
		return false;
	}

	return InitRenderer(renderer_cfg, wpd);
}

void Graphics::Shutdown()
{
	if (IsRendererInititalized())
		DestroyRenderer();

	if (IsMainWindowCreated())
		window_.Destroy();

	SystemWindow::ShutdownVideoSubsystem();
}

bool Graphics::PollSystemEvents()
{
	return window_.PollEvents();
}

void Graphics::RenderFrame()
{
	RendererPlugin* renderer = renderer_plugin_.GetInterface<RendererPlugin>();

	renderer->BeginFrame();

	renderer->EndFrame();
}

bool Graphics::InitRenderer(RendererConfig& renderer_cfg, const WindowPlatformData& main_wpd)
{
	const AvailableRenderer& renderer_record = renderers_list_[static_cast<unsigned>(renderer_cfg.renderer)];
	if (!renderer_plugin_.Load(renderer_record.filename, PluginType::RENDERER))
		return false;

	if (!renderer_plugin_.Create())
		return false;

	RendererPlugin* renderer = renderer_plugin_.GetInterface<RendererPlugin>();
	if (!renderer->InitializeMain(renderer_cfg, main_wpd))
		return false;

	return true;
}

void Graphics::DestroyRenderer()
{
	renderer_plugin_.Close();
}
} // namespace HT
