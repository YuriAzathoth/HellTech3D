/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GRAPHICS_GRAPHICS_H
#define GRAPHICS_GRAPHICS_H

#include <vector>
#include "Graphics/GraphicsConfig.h"
#include "Plugin/SharedLibraryPlugin.h"
#include "SystemWindow.h"

#define HT_RENDERER_LIBNAME_MAX_SIZE 128

namespace HT
{
class Graphics
{
public:
	Graphics();
	Graphics(Graphics&& other) noexcept;
	void operator=(Graphics&& other) noexcept;
	~Graphics();

	bool Initialize(WindowConfig& window_cfg, RendererConfig& renderer_cfg);
	void Shutdown();

	bool PollSystemEvents();
	void RenderFrame();

	bool IsMainWindowCreated() const { return window_.IsCreated(); }
	bool IsRendererInititalized() const { return renderer_plugin_.IsLoaded() && renderer_plugin_.IsCreated(); }

private:
	struct AvailableRenderer
	{
		char filename[HT_RENDERER_LIBNAME_MAX_SIZE];
	};
	
	bool InitRenderer(RendererConfig& renderer_cfg, const WindowPlatformData& main_window_pd);
	void DestroyRenderer();

	SystemWindow window_;
	SharedLibraryPlugin renderer_plugin_;
	std::vector<AvailableRenderer> renderers_list_;

	Graphics(const Graphics&) = delete;
	void operator=(const Graphics&) = delete;
};
} // namespace HT

#endif // GRAPHICS_GRAPHICS_H
