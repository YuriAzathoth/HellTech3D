/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef INPUT_INPUT_DEFS_H
#define INPUT_INPUT_DEFS_H

namespace HT
{
enum class KeyboardKeys
{
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	Key1,
	Key2,
	Key3,
	Key4,
	Key5,
	Key6,
	Key7,
	Key8,
	Key9,
	Key0,
	Return,
	Escape,
	Backspace,
	Tab,
	Space,
	Minus,
	Equals,
	LeftBracket,
	RightBracket,
	BackSlash,
	NonUsHash,
	Semicolon,
	Apostrophe,
	Grave,
	Comma,
	Period,
	Slash,
	F1,
	F2,
	F3,
	F4,
	F5,
	F6,
	F7,
	F8,
	F9,
	F10,
	F11,
	F12,
	PrintScreen,
	ScrollLock,
	Pause,
	Insert,
	Home,
	PageUp,
	Delete,
	End,
	PageDown,
	Right,
	Left,
	Down,
	Up,
	NumLock,
	NumDivide,
	NumMultiply,
	NumPlus,
	NumMinus,
	NumEnter,
	Num0,
	Num1,
	Num2,
	Num3,
	Num4,
	Num5,
	Num6,
	Num7,
	Num8,
	Num9,
	NumPeriod,
	NonUsBackSlash,
	// ---
	NumEquals,
	// ...
	Menu,
	// ...
	SysReq,
	// ...
	LCtrl,
	LShift,
	LAlt,
	LMeta,
	RCtrl,
	RShift,
	RAlt,
	RMeta,
	// ...
	_COUNT
};

enum class MouseButtons
{
	LEFT,
	RIGHT,
	WHEEL,
	X1,
	X2,
	_COUNT
};
} // namespace HT

#endif // INPUT_INPUT_DEFS_H
