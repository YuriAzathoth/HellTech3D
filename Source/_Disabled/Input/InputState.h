/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef INPUT_KEYBOARD_STATE_H
#define INPUT_KEYBOARD_STATE_H

#include <bitset>
#include "InputDefs.h"

namespace HT
{
struct KeyboardState
{
	std::bitset<static_cast<unsigned>(KeyboardKeys::_COUNT)> keys;
};

struct MouseState
{
	std::bitset<static_cast<unsigned>(MouseKeys::_COUNT)> buttons;
};

struct InputStateFrame
{
	KeyboardKeysState keys;
	MouseState buttons;
};

struct InputState
{
	InputStateFrame frames[2];
	InputStateFrame* current;
	InputStateFrame* previous;
};

struct KeyboardKeysState
{
};

struct KeyboardState
{
	SDL_ControllerAxisEvent::which
	KeyboardKeysState keys_current;
	KeyboardKeysState keys_previous;
};
} // namespace HT

#endif // INPUT_KEYBOARD_STATE_H
