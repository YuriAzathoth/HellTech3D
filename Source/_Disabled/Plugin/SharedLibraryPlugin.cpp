/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <new>
#include <utility>
#include "Plugin/PluginEntry.h"
#include "Plugin/PluginFactory.h"
#include "Plugin/PluginInterface.h"
#include "SharedLibraryPlugin.h"
#include "System/FileSystem.h"

namespace HT
{
SharedLibraryPlugin::SharedLibraryPlugin() :
	interface_(nullptr),
	factory_(nullptr)
{
}

SharedLibraryPlugin::SharedLibraryPlugin(SharedLibraryPlugin&& other) noexcept :
	interface_(other.interface_),
	factory_(other.factory_),
	lib_(std::move(other.lib_))
{
	other.interface_ = nullptr;
	other.factory_ = nullptr;
}

void SharedLibraryPlugin::operator=(SharedLibraryPlugin&& other) noexcept
{
	Destroy();
	Close();

	interface_ = other.interface_;
	other.interface_ = nullptr;

	factory_ = other.factory_;
	other.factory_ = nullptr;

	lib_ = std::move(other.lib_);
}

SharedLibraryPlugin::~SharedLibraryPlugin()
{
	Destroy();
	Close();
}

bool SharedLibraryPlugin::Load(const char* filename, PluginType type)
{
	Destroy();
	Close();

	if (!lib_.Load(HT_GetProgramPath(), filename))
		return false;

	using GetPluginEntryFN = const PluginEntry*();
	GetPluginEntryFN* GetPluginEntry =
		reinterpret_cast<GetPluginEntryFN*>(lib_.GetSymbol(HT_PLUGIN_ENTRY_FUNCTION_NAME));
	if (GetPluginEntry == nullptr)
	{
		lib_.Close();
		return false;
	}

	const PluginEntry* plugin_entry = (*GetPluginEntry)();
	if (plugin_entry == nullptr)
	{
		lib_.Close();
		return false;
	}
	
	for (unsigned i = 0; i < plugin_entry->factories_count; ++i)
	{
		if (plugin_entry->factories[i]->GetPluginType() == type)
		{
			factory_ = plugin_entry->factories[i];
			return true; // Typed plugin exists.
		}
	}

	// Typed plugin does not exist.
	lib_.Close();
	return false;
}

void SharedLibraryPlugin::Close()
{
	factory_ = nullptr;
	lib_.Close();
}

bool SharedLibraryPlugin::Create()
{
	if (factory_ == nullptr)
		return false;

	// TODO: Support stack allocator.	
	interface_ = reinterpret_cast<PluginInterface*>(::operator new(factory_->GetObjectSize(), std::nothrow));
	if (interface_ == nullptr) // If out of memory.
		return false;

	factory_->Create(interface_);
	return true;
}

void SharedLibraryPlugin::Destroy()
{
	if (interface_ != nullptr)
	{
		interface_->~PluginInterface();
		::operator delete(reinterpret_cast<void*>(interface_));
		interface_ = nullptr;
	}
}
} // namespace HT
