/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_SHARED_LIBRARY_PLUGIN_H
#define PLUGIN_SHARED_LIBRARY_PLUGIN_H

#include "HellTech3DCoreAPI.h"
#include "PluginDefs.h"
#include "System/SharedLibrary.h"

namespace HT
{
class PluginFactory;
class PluginInterface;

class HELLTECH3DCOREAPI_EXPORT SharedLibraryPlugin
{
public:
	SharedLibraryPlugin();
	SharedLibraryPlugin(SharedLibraryPlugin&& other) noexcept;
	void operator=(SharedLibraryPlugin&& other) noexcept;
	~SharedLibraryPlugin();

	bool Load(const char* filename, PluginType type);
	void Close();

	bool Create();
	void Destroy();

	template <typename T> T* GetInterface() { return static_cast<T*>(interface_); }
	template <typename T> const T* GetInterface() const { return static_cast<T*>(interface_); }

	PluginInterface* GetInterfaceBase() { return interface_; }
	const PluginInterface* GetInterfaceBase() const { return interface_; }

	bool IsLoaded() const { return factory_ != nullptr; };
	bool IsCreated() const { return interface_ != nullptr; };

private:
	PluginInterface* interface_;
	const PluginFactory* factory_;
	SharedLibrary lib_;

	SharedLibraryPlugin(const SharedLibraryPlugin&) = delete;
	void operator=(const SharedLibraryPlugin&) = delete;
};
} // namespace HT

#endif // PLUGIN_SHARED_LIBRARY_PLUGIN_H
