/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SYSTEM_PLATFORM_H
#define SYSTEM_PLATFORM_H

#include "System/CpuConfig.h"

#if defined(__CYGWIN__)
#	define HT_PLATFORM CYGWIN
#elif defined(__QNXNTO__)
#	define HT_PLATFORM QNXNTO
#elif defined(__APPLE__)
#	define HT_PLATFORM APPLE
#elif defined(WINCE)
#	define HT_PLATFORM WINCE
#elif defined(_WIN32)
#	define HT_PLATFORM W32
#elif defined(__native_client__)
#	define HT_PLATFORM NACL
#elif defined(__ANDROID__)
#	define HT_PLATFORM ANDROID
#elif defined(__linux)
#	define HT_PLATFORM LINUX
#elif defined(__unix)
#	define HT_PLATFORM UNIX
#else // defined
#	error Unsupported platform detected!
#endif // defined

#if defined(__INTEL_COMPILER)
#	define HT_COMPILER INTEL
#elif defined(__clang__)
#	if defined (__apple_build_version__)
#		define HT_COMPILER CLANG_APPLE
#	else // defined (__apple_build_version__)
#		define HT_COMPILER CLANG
#	endif // defined (__apple_build_version__)
#elif defined(_MSC_VER)
#	define HT_COMPILER MSVC
#elif defined(__GNUC__) || defined(__MINGW32__)
#	define HT_COMPILER GCC
#else // defined
#	error Unsupported compiler detected!
#endif // defined

#define HT_VECTORISATION_SSE 0x01
#define HT_VECTORISATION_SSE2 0x02
#define HT_VECTORISATION_SSE3 0x04
#define HT_VECTORISATION_SSSE3 0x08
#define HT_VECTORISATION_SSE41 0x10
#define HT_VECTORISATION_SSE42 0x20
#define HT_VECTORISATION_AVX 0x40
#define HT_VECTORISATION_AVX2 0x80

#if HT_VECTORISATION == NONE
#	define HT_ARCHITECTURE 0
#elif HT_VECTORISATION == SSE3
#	define HT_ARCHITECTURE HT_VECTORISATION_SSE | HT_VECTORISATION_SSE2 |\
		   HT_VECTORISATION_SSE3 | HT_VECTORISATION_SSSE3
#elif HT_VECTORISATION == SSE4
#	define HT_ARCHITECTURE HT_VECTORISATION_SSE | HT_VECTORISATION_SSE2 |\
		   HT_VECTORISATION_SSE3 | HT_VECTORISATION_SSSE3 | HT_VECTORISATION_SSE41|\
		   HT_VECTORISATION_SSE42
#elif HT_VECTORISATION == AVX
#	define HT_ARCHITECTURE HT_VECTORISATION_SSE | HT_VECTORISATION_SSE2 |\
		   HT_VECTORISATION_SSE3 | HT_VECTORISATION_SSSE3 | HT_VECTORISATION_SSE41|\
		   HT_VECTORISATION_SSE42 | HT_VECTORISATION_AVX
#elif HT_VECTORISATION == AVX2
#	define HT_ARCHITECTURE HT_VECTORISATION_SSE | HT_VECTORISATION_SSE2 |\
		   HT_VECTORISATION_SSE3 | HT_VECTORISATION_SSSE3 | HT_VECTORISATION_SSE41|\
		   HT_VECTORISATION_SSE42 | HT_VECTORISATION_AVX | HT_VECTORISATION_AVX2
#endif // HT_VECTORISATION

#ifdef HT_PLATFORM_MULTI_ARCH
	enum HT_PlatformArchitectures { HT_PLATFORM_MULTI_ARCH_ENUM };
#endif // HT_PLATFORM_MULTI_ARCH

#endif // SYSTEM_PLATFORM_H
