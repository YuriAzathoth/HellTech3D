/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include "SharedLibrary.hpp"
#include "System/BranchConfig.h"
#include "System/MicroArchitecture.h"

#define HT_SHARED_LIBRARY_MAX_PATH_SIZE 512

namespace HT
{
SharedLibrary::SharedLibrary() :
	handle_(nullptr)
{
}

SharedLibrary::SharedLibrary(SharedLibrary&& other) noexcept :
	handle_(other.handle_)
{
	other.handle_ = nullptr;
}

SharedLibrary& SharedLibrary::operator=(SharedLibrary&& other) noexcept
{
	Close();
	handle_ = other.handle_;
	other.handle_ = nullptr;
	return *this;
}

SharedLibrary::~SharedLibrary()
{
	Close();
}

bool SharedLibrary::Load(const char* full_filepath, const char* filename)
{
	if (handle_ != nullptr)
		return false;

	char full_filename[HT_SHARED_LIBRARY_MAX_PATH_SIZE];

	const char* arch_suffix = HT_GetMicroArchitectureSuffix(HT_PLATFORM_BRANCH_ARCHITECTURE);

	if (HT_SetSharedLibraryFullFilename(full_filename,
										HT_SHARED_LIBRARY_MAX_PATH_SIZE,
										full_filepath,
										strlen(full_filepath),
										filename,
										strlen(filename),
										arch_suffix,
										strlen(arch_suffix),
										0) == nullptr)
		return false;

	handle_ = HT_LoadSharedLibrary(full_filename);
	return handle_ != nullptr;
}

void SharedLibrary::Close()
{
	if (handle_ != nullptr)
	{
		HT_CloseSharedLibrary(handle_);
		handle_ = nullptr;
	}
}

void* SharedLibrary::GetSymbol(const char* symbol_name) const
{
	if (handle_ != nullptr)
		return HT_GetSharedLibrarySymbol(handle_, symbol_name);
	else
		return nullptr;
}
} // namespace HT
