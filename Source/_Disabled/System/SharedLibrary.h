/*
	HellTech3D - Game engine
	Copyright (C) 2024 Yuriy Zinchenko

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SYSTEM_SHARED_LIBRARY_HPP
#define SYSTEM_SHARED_LIBRARY_HPP

#include "HellTech3DCoreAPI.h"
#include "System/SharedLibrary.h"

namespace HT
{
class HELLTECH3DCOREAPI_EXPORT SharedLibrary
{
public:
	SharedLibrary();
	SharedLibrary(SharedLibrary&& other) noexcept;
	SharedLibrary& operator=(SharedLibrary&& other) noexcept;
	~SharedLibrary();

	bool Load(const char* full_filepath, const char* filename);
	void Close();

	void* GetSymbol(const char* symbol_name) const;

	template <typename T>
	T* GetSymbol(const char* symbol_name) const
	{
		return reinterpret_cast<T*>(GetSymbol(symbol_name));
	}

	bool IsLoaded() const { return handle_ != nullptr; }

private:
	HT_SharedLibrary handle_;

	SharedLibrary(const SharedLibrary&) = delete;
	SharedLibrary& operator=(const SharedLibrary&) = delete;
};
} // namespace HT

#endif // SYSTEM_SHARED_LIBRARY_HPP
